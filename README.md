

## Installation & Setup

* Clone this into `~/workspace/src/Variadassist/Web Player`.

Clone Variadassist web common into `~/workspace/src/Variadassist/web common`.

Make a link called "`common`" in `src/web`.

Add a `cabal.project` file and list this directory, the Haskell utils directory,
and the Variadassist Twitch auth directories as packages.

Install `mpg123` and `webfs`.

Organize sounds in `~/Twitch/sounds`.

* Add yourself to the `input` groups to allow the macro board device to be found and read.

```bash
sudo usermod -a -G input `whoami`
```

_This change will not take effect until you log out and log back in._ After relogging, use
`groups` to list the groups to which your user belongs nd confirm that `input` is among them.
