window.addEventListener("load", () => makeStatusType("goal", "top-right"));

const goalActions = {
	"fail goal": ({ goal }) => closeStatus("goal", goal, "cancelled"),
	"meet goal": ({ goal }) => closeStatus("goal", goal, "completed", "☑"),
	"set goal": ({ goal, display }) => makeStatus("goal", goal, display, "☐"),
};
