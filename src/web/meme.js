let memeDisplayCallout;
let memePreviewCallout;
let memePreviewQueueCounter;

let memePreviewQueue = [];

const showMemeIn = (memeCallout, { url, user }) =>
	hideMemeIn(memeCallout).then(() => new Promise((resolve, reject) => {
		const memeImg = memeCallout.querySelector("img");

		memeImg.src = url;
		memeImg.onload = () => {
			const memeUserContainer = memeCallout.querySelector(".meme-user");
			if (memeUserContainer)
				memeUserContainer.textContent = user;
			memeCallout.classList.add("shown");
			memeCallout.appendChild(memeImg);
			resolve("Showing image");
		};
	}));

const showMeme = meme => showMemeIn(memeDisplayCallout, meme);

const showMemePreview = meme => showMemeIn(memePreviewCallout, meme);

const hideMemeIn = memeCallout => {
	if (memeCallout.classList.contains("shown"))
		return transitionCSS(memeCallout, ["shown"], ["shown", "leaving"])
			.then(() => awaitAnimation(memeCallout))
			.then(() => transitionCSS(memeCallout, ["shown", "leaving"], []));
	else
		return Promise.resolve("meme is already hidden!");
};

const hideMeme = () => hideMemeIn(memeDisplayCallout);

const hideMemePreview = () => hideMemeIn(memePreviewCallout);

const updateMemeQueueCounter = () => {
	if (memePreviewQueue.length <= 1) {
		memePreviewQueueCounter.classList.remove("shown");
	} else {
		memePreviewQueueCounter.innerHTML = "+" + (memePreviewQueue.length - 1);
		memePreviewQueueCounter.classList.add("shown");
	}
};

const previewMeme = meme => {
	if (memePreviewQueue.length === 0)
		showMemePreview(meme);
	memePreviewQueue.push(meme);
	updateMemeQueueCounter();
};

const dequeueMeme = () => {
	memePreviewQueue.shift();
	if (memePreviewQueue.length === 0)
		hideMemePreview();
	else
		showMemePreview(memePreviewQueue[0]);
	updateMemeQueueCounter();
};

const queueMemes = onError => queuedHaltableSequence(meme => [
	{
		run: () => showMeme(meme),
		onHalt: () => transitionCSS(memeDisplayCallout, ["shown"], []),
	},
	{
		run: () => transitionCSS(memeDisplayCallout, ["shown"])
			.then(() => awaitAnimation(memeDisplayCallout)),
		onHalt: () => transitionCSS(memeDisplayCallout, ["shown"], []),
	},
], onError);

const memeActions = () => {
	memePreviewCallout = document.getElementById("next-meme-callout");
	memePreviewQueueCounter = document.getElementById("meme-queue-counter");
	memeDisplayCallout = document.getElementById("shown-meme-callout");

	return {
		"meme": showMeme,
		"hide meme": hideMeme,
	};
};

const memePreviewActions = {
	"preview meme": previewMeme,
	"dequeue meme": dequeueMeme,
};