let chatMessagesScrollBox;

const initChatMessagesScrollBox = () => {
	if (!chatMessagesScrollBox) {
		const chatContainer = document.getElementById("chat-container");
		chatMessagesScrollBox = newScrollBox(chatContainer);
	}

	return chatMessagesScrollBox;
};

const displayChannelPointsRedemption = ({ user, reward, message, priority }) => {
	initChatMessagesScrollBox();

	chatMessagesScrollBox.enqueue(
		`${user} used ${reward}` + (message ? `: "${message}"` : ""),
		["top-left-callout", "channel-points-redemption"].concat(priority ? ["priority"] : []))
}

const displayChatMessage = (origin, message) => {
	initChatMessagesScrollBox();

	chatMessagesScrollBox.enqueue(`${origin}: ${message}`, ["top-left-callout", "chat-message"]);
};

const chatActions = {
	"from chat": ({ origin, message }) => displayChatMessage(origin, message),
	"redeem Channel Points": displayChannelPointsRedemption,
};
