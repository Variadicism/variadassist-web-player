let sendToServer = () => console.warn("No way to send the server was set!");

const _messageServer = (actionType, sendToConsole, message, relatedActionMessage = null) => {
	sendToConsole(relatedActionMessage ?
		`${message}\nrelated action message:${JSON.stringify(relatedActionMessage)}` : message);
	console.trace();

	sendToServer(JSON.stringify({
		action: actionType,
		message,
		...(relatedActionMessage ? { relatedActionMessage } : {}),
	}));
};

const err = (message, relatedActionMessage = null) =>
	_messageServer("error", console.error, message, relatedActionMessage);

const warn = (message, relatedActionMessage = null) =>
	_messageServer("warning", console.warn, message, relatedActionMessage);
