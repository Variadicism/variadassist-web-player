/*
 * Timer state:
 * {
 *   duration: number (non-negative integer, seconds),
 *   timeoutID: ID,
 *   timeRemaining: number (non-negative integer, seconds)
 * }
 */

const LEAVE_DELAY_AFTER_ELAPSED = 7;
const LEAVE_DELAY_AFTER_CANCELLED = 3;

window.addEventListener("load", () => makeStatusType("timer", "top-left"));

const formatTime = duration => {
	const hours = Math.floor(duration/3600);
	const hoursString = duration > 3600 ? `${hours}:` : "";
	const minutes = Math.floor((duration%3600)/60);
	const minutesString = (hours > 0 && minutes < 10 ? "0" : "") + minutes + ":";
	const seconds = Math.floor(duration%60);
	const secondsString = (seconds < 10 ? "0" : "") + seconds;
	return hoursString + minutesString + secondsString;
};

const makeTimer = (id, displayName, duration) =>
	makeStatus("timer", id, displayName, formatTime(duration),
		{ duration, timeoutID: null, timeRemaining: duration });

const renameTimer = (id, newName) => renameStatus("timer", id, newName);

const runTimer = ({ container, id }) => {
	const updateTimer = () => updateStatus("timer", id, state => {
		if (state.timeRemaining > 0)
			return {
				fill: (state.duration - state.timeRemaining) * 100.0 / state.duration,
				indicator: formatTime(state.timeRemaining),
				state: {
					...state,
					timeRemaining: state.timeRemaining - 1,
					timeoutID: setTimeout(updateTimer, 1000),
				},
			};
		else {
			closeStatus("timer", id, "completed", "😊", LEAVE_DELAY_AFTER_ELAPSED);
			return {};
		}
	});
	updateTimer();
};

const restartTimer = ({ duration }) => ({ state: { timeRemaining: duration } });

const cancelTimer = name => {
	findStatusThen("timer", name, state => {
		clearTimeout(state.timeoutID);
		return { ...state, timeoutID: null };
	});
	closeStatus("timer", name, "cancelled", "-:--");
};

const timerActions = {
	"make timer": ({ name, displayName, duration }) => makeTimer(name, displayName, duration),
	"prototype timer": ({ name, duration }) => makeTimer(name, "", duration),
	"name prototype timer": ({ name, displayName }) => renameStatus("timer", name, displayName),
	"start timer": ({ name }) => findStatusThen("timer", name, runTimer),
	"restart timer": ({ name }) => updateStatus("timer", name, restartTimer),
	"cancel timer": ({ name }) => cancelTimer(name),
};
