const TEXT_TO_SPEECH_START_DELAY_SEC = 0.75;  // including shown transition
const TEXT_TO_SPEECH_END_BUFFER_SEC = 0.75;  // including leaving animation, but not possible highlight timeout
const TEXT_TO_SPEECH_FLASH_PERIODICITY_SEC = 0.75;

let currentTextToSpeechFinished = false;
const textToSpeechQueue = queuedHaltableSequence(({ message, user }) => {
	const ttsCallout = document.getElementById("tts-callout");
	const ttsUser = ttsCallout.querySelector(".user");
	const ttsMessage = ttsCallout.querySelector(".message");

	return [
		{
			run: () => {
				currentTextToSpeechFinished = false;
				ttsUser.textContent = user;
				ttsMessage.textContent = message;
				ttsCallout.classList.add("shown");
			},
		},
		{
			delay: TEXT_TO_SPEECH_START_DELAY_SEC,
			run: () => {
				return new Promise(resolve => {
					const loopToggleHighlight = () => setTimeout(() => {
						if (currentTextToSpeechFinished || ttsCallout.classList.contains("highlighted"))
							ttsCallout.classList.remove("highlighted");
						else
							ttsCallout.classList.add("highlighted");

						if (!currentTextToSpeechFinished)
							loopToggleHighlight();
						else {
							ttsCallout.classList.add("leaving");
							// Do not return or await these to keep the end buffer time whole.
							awaitAnimation(ttsCallout).then(
								() => ttsCallout.classList.remove("shown", "leaving"));
							resolve("Done!");
						}
					}, TEXT_TO_SPEECH_FLASH_PERIODICITY_SEC * 1000);
					loopToggleHighlight();
				});
			},
		},
		{
			delay: TEXT_TO_SPEECH_END_BUFFER_SEC,
			run: () => {},
		},
	]
});

const textToSpeechActions = {
	"show text to speech": textToSpeechQueue.enqueue,
	"text to speech finished": () => currentTextToSpeechFinished = true,
};
