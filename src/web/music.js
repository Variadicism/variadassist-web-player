let musicCalloutsSequence;

const showMusicCallouts = (musicInfoDisplayTimeSec, { category, subcategory, songArtist, songTitle }) => {
	const categoryCallout = document.getElementById("music-category-callout");
	const songCallout = document.getElementById("song-callout");
	const stopCallout = document.getElementById("stop-music-callout");
	const allCallouts = [categoryCallout, songCallout, stopCallout];

	const calloutsToShow = songArtist ? category ? [categoryCallout, songCallout] :
		[songCallout] : [stopCallout];
	const calloutsToHide = allCallouts.filter(c => !calloutsToShow.includes(c));

	const musicType = subcategory ? `${subcategory} (${category})` : category;
	const categoryMessage = `Playing ${musicType} music...`;
	const songMessage = `"${songTitle}" by ${songArtist}`;

	if (musicCalloutsSequence)
		musicCalloutsSequence.halt();

	musicCalloutsSequence = haltableSequence([
		{
			run: () => {
				if (calloutsToShow.some(c => c.classList.contains("shown")))
					return hide(...allCallouts);
			},
		},
		{
			onHalt: () => hide(...calloutsToShow),
			run: () => {
				if (category)
					categoryCallout.textContent = categoryMessage;
				if (songTitle)
					songCallout.textContent = songMessage;

				return Promise.all([
					show(...calloutsToShow),
					hide(...calloutsToHide),
				]);
			},
		},
		...(musicInfoDisplayTimeSec <= 0 ? [] : [{
				delay: musicInfoDisplayTimeSec,
				run: () => hide(...calloutsToShow),
			},
		]),
	]);
	musicCalloutsSequence.run();
};

const musicActions = musicInfoDisplayTimeSec => {
	const categoryCallout = document.getElementById("music-category-callout");
	const songCallout = document.getElementById("song-callout");
	const stopCallout = document.getElementById("stop-music-callout");

	const categoryCalloutMessage = document.querySelector("#music-category-callout .message");
	const songCalloutMessage = document.querySelector("#song-callout .message");
	const stopCalloutMessage = document.querySelector("#stop-music-callout .message");

	return {
		"music category": message => showMusicCallouts(musicInfoDisplayTimeSec, message),
		"song": message => showMusicCallouts(musicInfoDisplayTimeSec, message),
		"stop music": () => showMusicCallouts(musicInfoDisplayTimeSec, {}),
	};
};
