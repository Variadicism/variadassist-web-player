const RANDOMIZER_OPTION_APPEAR_DELAY_SEC = 0.5;
const RANDOMIZER_WINNER_DELAY_SEC = 8;
const RANDOMIZER_REWEIGH_DELAY_SEC = 5;
const RANDOMIZER_HIDE_DELAY_SEC = 20;
const RANDOMIZER_LIGHTWEIGHT_THRESHOLD = 5;

const classifyOption = (optionContainer, weights, i) => {
	const weight = weights[i];
	const randomizerOption = optionContainer.querySelector(".randomizer-option");
	optionContainer.style.setProperty("--weight", weight);
	if (weight < RANDOMIZER_LIGHTWEIGHT_THRESHOLD) {
		if (weight === 0)
			optionContainer.classList.add("weightless");
		else
			optionContainer.classList.remove("weightless");
		if (i > 0 && (weights[i - 1].weight < RANDOMIZER_LIGHTWEIGHT_THRESHOLD ||
				i === weights.length - 1)) {
			optionContainer.classList.add("lightweight", "lowered");
			randomizerOption.classList.add("top-left-callout");
			randomizerOption.classList.remove(
				"bottom-left-callout", "top-right-callout");
		} else {
			optionContainer.classList.add("lightweight");
			optionContainer.classList.remove("lowered");
			randomizerOption.classList.add("bottom-left-callout");
			randomizerOption.classList.remove(
				"top-left-callout", "top-right-callout");
		}
	} else {
		randomizerOption.classList.add("top-right-callout");
		optionContainer.classList.remove(
			"weightless", "lightweight");
		randomizerOption.classList.remove(
			"bottom-left-callout", "top-left-callout");
	}

	randomizerOption.querySelector(".randomizer-weight").textContent = weight.toFixed(1);
}

const classifyDivider = (divider, weights, weightIndex) => {
	if (divider && weights[weightIndex - 1] > 0
			&& weights.some((weight, i) => weight > 0 && i >= weightIndex))
		divider.classList.add("shown");
	else
		divider.classList.remove("shown");
}

const classifyTag = (weightTag, weight) => {
	if (weight > RANDOMIZER_LIGHTWEIGHT_THRESHOLD)
		weightTag.classList.add("callout", "bottom-right-callout", "shown");
	else
		weightTag.classList.remove("callout", "bottom-right-callout");
};

const showRandomizer = ({ name, entries, weights, showTitles }, spinResults = undefined) => {
	const randomizerContainer = document.getElementById("randomizer-container");
	const randomizerWinnerContainer = document.getElementById("randomizer-winner-container");
	const randomizerWinnerCallout = document.getElementById("randomizer-winner-callout");

	const makeSpinSteps = () => {
		if (!spinResults) return [];

		const { winner, spectrumValue, newWeights } = spinResults;

		return [
			{
				delay: RANDOMIZER_WINNER_DELAY_SEC,
				run: () => {
					randomizerWinnerContainer.style.setProperty("--spectrumValue", spectrumValue);
					randomizerWinnerContainer.classList.add("shown");
					randomizerWinnerCallout.classList.add("shown");
					return awaitAnimation(randomizerWinnerContainer);
				},
			},
			{
				run: () => randomizerOptions.forEach((option, i) => {
						if (entries[i] === winner)
							option.classList.add("winner");
					}),
			},
			{
				delay: RANDOMIZER_REWEIGH_DELAY_SEC,
				run: () => {
					randomizerWinnerContainer.classList.add("leaving");
					// TODO: when new entries are added, create more entries.
					randomizerOptions.forEach((option, i) =>
						classifyOption(option, newWeights, i));
					randomizerDividers.forEach((divider, i) =>
						classifyDivider(divider, newWeights, i + 1));
					randomizerWeightTags.forEach((tag, i) =>
						classifyTag(tag, newWeights[i]));
					randomizerTitleTags.forEach((tag, i) =>
						classifyTag(tag, newWeights[i]));
					return awaitAnimation(randomizerWinnerContainer);
				},
			},
			{
				run: () => randomizerWinnerContainer.classList.remove("shown", "leaving"),
			}]
	};

	let randomizerOptions = [];
	let randomizerDividers = [];
	let randomizerWeightTags = [];
	let randomizerTitleTags = [];
	haltableSequence([
		{
			run: () => randomizerContainer.classList.add("shown"),
		},
		...weights.map((weight, i) => ({
			delay: i === 0 ? 0 : RANDOMIZER_OPTION_APPEAR_DELAY_SEC,
			run: () => {
				const entry = entries[i];

				let divider;
				if (i > 0) {
					divider = document.createElement("div");
					divider.classList.add("randomizer-divider");
					randomizerDividers.push(divider);
					randomizerContainer.appendChild(divider);
				}

				const optionContainer = document.createElement("div");
				optionContainer.classList.add("randomizer-option-container");

				const randomizerOption = document.createElement("div");
				randomizerOption.classList.add("randomizer-option", "callout");

				const randomizerOptionImage = document.createElement("img");
				randomizerOptionImage.classList.add("randomizer-option-image");
				const urlEscapeSpaces = string => string.replace(" ", "%20");
				
				randomizerOptionImage.src =
					urlEscapeSpaces(`http://localhost:7043/static/${name}/${entry}.png`);
				randomizerOption.appendChild(randomizerOptionImage);

				const titleDisplay = document.createElement("div");
				titleDisplay.classList.add("randomizer-tag", "randomizer-title");
				if (showTitles) {
					titleDisplay.textContent = entry;
					classifyTag(titleDisplay, weight);
				}
				randomizerTitleTags.push(titleDisplay);
				randomizerOption.appendChild(titleDisplay);

				const weightTag = document.createElement("div");
				weightTag.classList.add("randomizer-tag", "randomizer-weight");
				classifyTag(weightTag, weight);
				randomizerWeightTags.push(weightTag);
				randomizerOption.appendChild(weightTag);

				optionContainer.appendChild(randomizerOption);
				randomizerOptions.push(optionContainer);
				randomizerContainer.appendChild(optionContainer);

				classifyOption(optionContainer, weights, i);

				optionContainer.classList.add("shown");
				randomizerOption.classList.add("shown");
				if (divider) classifyDivider(divider, weights, i);
				return awaitAnimation(optionContainer);
			},
		})),
		...makeSpinSteps(),
		{
			delay: RANDOMIZER_HIDE_DELAY_SEC,
			run: () => {
				randomizerContainer.classList.add("leaving");
				return awaitAnimation(randomizerContainer);
			},
			onHaltContinue: true,
		},
		{
			run: () => {
				[...randomizerOptions, ...randomizerDividers].map(o =>
					randomizerContainer.removeChild(o));
				randomizerContainer.classList.remove("shown", "leaving");
			},
			onHaltContinue: true,
		}], console.error).run();
};

const randomizerActions = {
	"display randomizer": showRandomizer,
	"spin randomizer": ({ newWeights, winner, spectrumValue, ...basicRandomizerInfo }) =>
		showRandomizer(basicRandomizerInfo, { newWeights, winner, spectrumValue }),
};
