module Variadassist.WebPlayer.OverlayAlerts (
	OverlayAlert(..),
	overlayAlerts,
	overlayAlertSoundsDir,
	dequeueOverlayAlert,
	enqueueOverlayAlert,
	enqueueOverlayAlertByName,
	inquireThenEnqueueOverlayAlert
) where

import Utils.Choose (requestChoice)
import Utils.Fallible
import Utils.Fallible.Trans
import Utils.HATs
import Utils.Messaging hiding (err, message, warn)
import Utils.Queues

import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Types.OverlayAlerts
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Utils (playSound)
import Variadassist.WebPlayer.Web.Helix (fulfillRedemption)
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(..))
import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption(..))
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

import Control.Concurrent (threadDelay)
import Data.List (find)
import Data.String.Utils (strip)
import System.FilePath ((</>))

-- constants

overlayAlertGapSeconds :: Int
overlayAlertGapSeconds = 1

overlayAlerts :: [OverlayAlert]
overlayAlerts = [
	OverlayAlert {
		oaName = "Illuminati",
		oaImageURL = "file:///home/variadicism/Twitch/pictures/alerts/Illuminati.png",
		oaSoundPath = Just $ overlayAlertSoundsDir </> "Illuminati.mp3",
		oaVolume = 25
	},
	OverlayAlert {
		oaName = "Technoblade bruh",
		oaImageURL = "https://i.redd.it/jq328tfjdt061.png",
		oaSoundPath = Just $ overlayAlertSoundsDir </> "Technoblade bruh.mp3",
		oaVolume = 30
	},
	OverlayAlert {
		oaName = "One More Thing",
		oaImageURL = "https://c.tenor.com/bnAH7qowLXcAAAAC/one-more-thing-uncle-chan.gif",
		oaSoundPath = Just $ overlayAlertSoundsDir </> "One More Thing.mp3",
		oaVolume = 35
	},
	OverlayAlert {
		oaName = "Villager sigh",
		oaImageURL = "https://pbs.twimg.com/profile_images/1223051067824340992/jMFwQ5oO_400x400.jpg",
		oaSoundPath = Just $ overlayAlertSoundsDir </> "Villager sigh.mp3",
		oaVolume = 45
	},
	OverlayAlert {
		oaName = "JJBA To Be Continued",
		oaImageURL = "https://purepng.com/public/uploads/large/to-be-continued-meme-un6.png",
		oaSoundPath = Just $ overlayAlertSoundsDir </> "JJBA To Be Continued.mp3",
		oaVolume = 35
	},
	OverlayAlert {
		oaName = "Vlad \"Now GOOOOO!\"",
		oaImageURL = "file:///home/variadicism/Twitch/pictures/alerts/Vlad throne.png",
		oaSoundPath = Just $ overlayAlertSoundsDir </> "Now Go Bleigh.mp3",
		oaVolume = 100
	},
	OverlayAlert {
		oaName = "Excellent",
		oaImageURL = "file:///home/variadicism/Twitch/pictures/alerts/Excellent.gif",
		oaSoundPath = Just $ overlayAlertSoundsDir </> "Excellent.mp3",
		oaVolume = 60
	}]

overlayAlertSoundsDir :: FilePath
overlayAlertSoundsDir = "/home/variadicism/Twitch/sounds/alerts"

-- exported utils

dequeueOverlayAlert :: VoidFallible
dequeueOverlayAlert = do
	State{..} <- tryIO getState
	case fst $ dequeue overlayAlertsQueue of
		Nothing -> debugM "No more overlay alerts are enqueued."
		Just oar -> do
			let OverlayAlert{..} = oarAlert oar
			debugM $ "Playing next overlay alert " ++ msg oaName ++ "..."
			haltableAsync
					("play overlay alert sound " ++ msg oaSoundPath ++ ", then next") $ do
				debugM $ "Showing next overlay alert, " ++ msg oaName
				sendJSONToPanels ShowOverlayAlertWebMessage {
					soawmName = oaName,
					soawmImageURL = oaImageURL }
				maybe success (flip playSound oaVolume) oaSoundPath
				maybe success fulfillRedemption $ oarRR oar
				debugM "Done playing overlay alert sound; spacing out next overlay alert slot..."
				tryIO $ threadDelay (overlayAlertGapSeconds * 1000000)
				debugM $ "Checking for queued overlay alerts..."
				tryIO $ statefully $
					\state@State{..} -> state{ overlayAlertsQueue = snd $ dequeue overlayAlertsQueue }
				dequeueOverlayAlert
			success

enqueueOverlayAlert :: OverlayAlert -> Maybe RewardRedemption -> VoidFallible
enqueueOverlayAlert overlayAlert@OverlayAlert{..} maybeRR = do
	debugM $ "Enqueueing overlay " ++ msg oaName ++ "..."
	let overlayAlertRedemption = case maybeRR of
		Nothing -> MyOverlayAlert overlayAlert
		Just rr -> OverlayAlertReward overlayAlert rr

	queueWasEmpty <- tryIO $ statefullyOutputting $ \state@State{..} ->
		(state{ overlayAlertsQueue = enqueue overlayAlertsQueue overlayAlertRedemption }, null overlayAlertsQueue)
	if queueWasEmpty then do
		debugM $ "First overlay alert enqueued; starting dequeueing..."
		dequeueOverlayAlert
	else success

enqueueOverlayAlertByName :: String -> Maybe RewardRedemption -> VoidFallible
enqueueOverlayAlertByName name maybeRR = do
	overlayAlert <- falT $
		fromMaybe ("There is no " ++ msg name ++ " overlay alert!") $
		find ((name==) . oaName) overlayAlerts
	enqueueOverlayAlert overlayAlert maybeRR

inquireThenEnqueueOverlayAlert :: VoidFallible
inquireThenEnqueueOverlayAlert = do
	maybeName <- requestChoice $ map oaName overlayAlerts
	case maybeName of
		Nothing -> debugM "No overlay alert name was given!"
		Just name -> enqueueOverlayAlertByName (strip name) Nothing
