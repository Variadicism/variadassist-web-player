module Variadassist.WebPlayer.Music (
	musicVolume,
	musicDir,
	inquireThenPlayMusicCategory,
	loopShuffle,
	makeCategoryPlaylist,
	playMusicCategory,
	stopMusic,
) where

import Utils.Choose (requestChoice)
import Utils.Fallible.Trans
import Utils.HATs
import Utils.Lists
import Utils.Messaging hiding (err, message, warn)

import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Types.Music
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Utils
import Variadassist.WebPlayer.Web.ControlPanel (message, warn)
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(..))
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

import Control.Monad (filterM)
import qualified Data.Map.Ordered as OMap
import qualified Data.Text.Lazy as Text
import System.Directory (doesDirectoryExist, listDirectory)
import System.FilePath ((</>))
import System.Random (getStdGen, randomR)
import Sound.HTagLib (artistGetter, getTags, titleGetter, unArtist, unTitle)

-- constants

musicVolume = 10

musicDir :: FilePath
musicDir = "/home/variadicism/Twitch/sounds/music/by action setting"

-- private utils

getAudioInfo :: FilePath -> IO TrackInfo
getAudioInfo filePath = do
	debugM $ "Getting track info for " ++ filePath ++ "..."
	trackInfo <- getTags filePath $ TrackInfo <$> titleGetter <*> artistGetter
	debugM $ "Found track info: " ++ show trackInfo
	return trackInfo

stopMusicOnly :: VoidFallible
stopMusicOnly = do
	State{..} <- tryIO getState
	case maybeMusicTask of
		Nothing -> debugM "No previous player task to halt."
		Just musicTask -> haltTask "New category selected" musicTask

shufflePlaylist :: Playlist -> IO Playlist
shufflePlaylist playlist = do
	shuffledAssocs <- shuffle $ OMap.assocs playlist
	return $ OMap.fromList shuffledAssocs

makeCategoryPlaylist :: MusicCategory -> IO Playlist
makeCategoryPlaylist MusicCategory{..} = do
	rng <- getStdGen
	let categoryDir = musicDir </> musicCategory
	categoryDirContents <- listDirectory categoryDir
	categorySongPaths <- filterM (fmap not . doesDirectoryExist) $ map (categoryDir </>) categoryDirContents
	debugM $ "Chose category songs: " ++ msgList categorySongPaths
	subcategorySongPaths <- case musicSubcategory of
		Nothing -> do
			debugM "No subcategory; skipping choosing..."
			return []
		Just subcategory -> do
			let subcategoryDir = categoryDir </> subcategory
			subcategorySongPaths <- fmap (map (subcategoryDir </>)) $ listDirectory subcategoryDir
			debugM $ "Chose subcategory songs: " ++ msgList subcategorySongPaths
			return subcategorySongPaths
	startSongPathList <-
		if musicStarting then do
			let startDir = categoryDir </> "START"
			startDirExist <- doesDirectoryExist startDir
			if startDirExist then do
				startSongPaths <- fmap (map (startDir </>)) $ listDirectory startDir
				let startSongPath = startSongPaths !! (fst $ randomR (0, length startSongPaths - 1) rng)
				debugM $ "Chose start song: " ++ msg startSongPath
				return [startSongPath]
			else do
				debugM "Start folder missing! Ignoring start song choice..."
				return []
		else do
			debugM "No start songs; skipping choosing..."
			return []

	let songPaths = startSongPathList ++ shuffleWith rng subcategorySongPaths ++ shuffleWith rng categorySongPaths

	debugM "Finding song info..."
	let pairWithInfo = \songPath -> fmap (songPath,) $ getAudioInfo songPath
	songsWithInfo <- mapM pairWithInfo songPaths
	debugM $ "Song info found! Playlist populated: " ++ msgShowList songsWithInfo
	return $ OMap.fromList songsWithInfo

playSong :: FilePath -> VoidFallible
playSong songPath = do
	playSound songPath musicVolume
	haltIfNeeded

playAndMessageSongs :: Playlist -> VoidFallible
playAndMessageSongs playlist = case playlist `OMap.elemAt` 0 of
	Nothing -> success
	Just (songPath, songInfo) -> do
		sendJSONToPanels SongWebMessage{
			swmSongTitle = Text.unpack $ Text.fromStrict $ unTitle $ trackTitle songInfo,
			swmSongArtist = Text.unpack $ Text.fromStrict $ unArtist $ trackArtist songInfo }
		playSong songPath
		playAndMessageSongs (OMap.delete songPath playlist)

loopShuffle :: Playlist -> VoidFallible
loopShuffle oldPlaylist = do
	playlist <- tryIO $ shufflePlaylist oldPlaylist
	debugM $ "Playlist reshuffled: " ++ (msgList $ map fst $ OMap.assocs playlist)
	message "Playlist finished; playing reshuffled..."
	playAndMessageSongs playlist
	loopShuffle playlist

loopShuffleCategoryPlaylist :: Playlist -> MusicCategory -> VoidFallible
loopShuffleCategoryPlaylist playlist MusicCategory{..} = case playlist `OMap.elemAt` 0 of
	Nothing -> warn "Category playlist empty!"
	Just (firstSongPath, firstSongInfo) -> do
		debugM "Sending category web message..."
		sendJSONToPanels MusicCategoryWebMessage{
			cwmCategory = musicCategory,
			cwmSubcategory = musicSubcategory,
			cwmStarting = musicStarting,
			cwmSongTitle = Text.unpack $ Text.fromStrict $ unTitle $ trackTitle firstSongInfo,
			cwmSongArtist = Text.unpack $ Text.fromStrict $ unArtist $ trackArtist firstSongInfo }
		debugM $ "Playing first song \"" ++ firstSongPath ++ "\"..."
		playSong firstSongPath
		debugM $ "Playing remaining songs with song web messages..."
		playAndMessageSongs (OMap.delete firstSongPath playlist)
		-- Remove the "starting" song if there is one.
		let songsToShuffleModifier = if musicStarting then OMap.delete firstSongPath else id
		debugM $ "Reshuffling and replaying..."
		loopShuffle (songsToShuffleModifier playlist)

-- exported utils

stopMusic :: VoidFallible
stopMusic = do
	debugM "Stopping music..."
	stopMusicOnly
	sendJSONToPanels StopMusicWebMessage
	tryIO $ statefully $ \state -> state{ maybeMusicTask = Nothing }

playMusicCategory :: MusicCategory -> VoidFallible
playMusicCategory category@MusicCategory{..} = do
	debugM "Stopping music for new category..."
	stopMusicOnly
	debugM $ "Playing category as " ++ msgList ([musicCategory, show musicSubcategory, show musicStarting])
	playlist <- tryIO $ makeCategoryPlaylist category
	debugM $ "Playlist created: " ++ (msgList $ map fst $ OMap.assocs playlist)
	newMusicTask <- haltableAsync ("playing music category " ++ msgList ([musicCategory, show musicSubcategory, show musicStarting])) $
		loopShuffleCategoryPlaylist playlist category
	tryIO $ statefully $ \state -> state{ maybeMusicTask = Just newMusicTask }

inquireThenPlayMusicCategory :: VoidFallible
inquireThenPlayMusicCategory = do
	musicCategories <- tryIO $ listDirectory musicDir
	maybeCategory <- requestChoice $ "[STOP]":musicCategories
	case maybeCategory of
		Nothing -> debugM "No music category was chosen!"
		Just "[STOP]" -> do
			debugM "Music stop chosen!"
			stopMusic
		Just category -> do
			debugM $ "Category " ++ msg category ++ " chosen; analyzing subcategories..."
			categoryDirContents <- tryIO $ listDirectory $ musicDir </> category
			subcategories <- tryIO $ filterM
				(doesDirectoryExist . (musicDir </>) . (category </>)) categoryDirContents
			debugM $ "Subcategories found: " ++ msgList subcategories
			maybeSubcategoryAndStarting <-
				if null subcategories then do
					debugM "No subcategories; assembling play music category command..."
					return $ Just (Nothing, False)
				else do
					maybeSubcategoryChoice1 <- requestChoice $ "[]":subcategories
					case maybeSubcategoryChoice1 of
						Nothing -> do
							debugM "No subcategory chosen!"
							return Nothing
						Just "[]" -> do
							debugM "Null subcategory chosen!"
							return $ Just (Nothing, False)
						Just subcategoryChoice1 -> do
							debugM $ "Subcategory " ++ msg subcategoryChoice1 ++ " chosen"
							if subcategoryChoice1 /= "START" then do
								debugM $ "Playing music with subcategory " ++ msg subcategoryChoice1 ++ "..."
								return $ Just (Just subcategoryChoice1, False)
							else do
								debugM "START subcategory chosen; prompting for second subcategory..."
								let subcategoryChoices = filter (/="START") subcategories
								if null subcategoryChoices then do
									debugM "No other subcategories available; playing music with START..."
									return $ Just (Nothing, True)
								else do
									maybeSubcategory <- requestChoice $ "[]":subcategoryChoices
									case maybeSubcategory of
										Nothing -> do
											debugM "No subcategory chosen!"
											return Nothing
										Just "[]" -> do
											debugM "Null subcategory chosen!"
											return $ Just (Nothing, True)
										Just subcategoryChoice2 -> do
											debugM $ msg subcategoryChoice2 ++ " subcategory chosen; playing music with START..."
											return $ Just (Just subcategoryChoice2, True)
			case maybeSubcategoryAndStarting of
				Nothing -> debugM "Play music category cancelled!"
				Just (subcategory, starting) -> do
					let chosenCategory = MusicCategory {
						musicCategory = category,
						musicSubcategory = subcategory,
						musicStarting = starting }
					debugM $ "Play music category choices accepted: " ++ show chosenCategory
					playMusicCategory chosenCategory
