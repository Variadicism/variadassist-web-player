module Variadassist.WebPlayer.Paths (
	homeDir,
	variadassistDir,
	webCommonDir,
	chromeSessionsDir,
	webPlayerDir,
	webFilesDir,
	dataDir,
	viewerDataPath,
	tempDir,
	soundEffectsDir,
	highlightsDir,
	markersDir
) where

import System.FilePath ((</>))

homeDir :: FilePath
homeDir = "/home/variadicism"

variadassistDir :: FilePath
variadassistDir = homeDir </> "workspace/src/Variadassist"

webCommonDir :: FilePath
webCommonDir = variadassistDir </> "web common"

chromeSessionsDir :: FilePath
chromeSessionsDir = webCommonDir </> "Chrome sessions"

webPlayerDir :: FilePath
webPlayerDir = variadassistDir </> "Web Player"

webFilesDir :: FilePath
webFilesDir = webPlayerDir </> "src/web"

dataDir :: FilePath
dataDir = webPlayerDir </> "data"

viewerDataPath :: FilePath
viewerDataPath = dataDir </> "viewers.hsd"

tempDir :: FilePath
tempDir = "/tmp/Variadassist"

twitchDir :: FilePath
twitchDir = homeDir </> "Twitch"

soundEffectsDir :: FilePath
soundEffectsDir = twitchDir </> "sounds/effects"

highlightsDir :: FilePath
highlightsDir = twitchDir </> "videos/highlights"

markersDir :: FilePath
markersDir = highlightsDir </> "markers"