module Main where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.Files (readHaskellDataFileOr, serveDirHTTP, writeHaskellDataFile)
import Utils.HATs (haltableAsync)
import Utils.Messaging hiding (err, message, warn)
import qualified Utils.Messaging as Messaging
import Utils.Processes hiding (message)
import Utils.Queues
import Utils.WebSockets (WebSocket, close)
import Variadassist.Twitch.Secrets (streamerUsername)

import Variadassist.WebPlayer.Chat (initChat, leaveChat)
import Variadassist.WebPlayer.Commands (commandPipePath, executeCommand, readPipeCommand)
import Variadassist.WebPlayer.MacroBoard (captureMacros, defaultMacroBoardMode, displayMode)
import Variadassist.WebPlayer.Memes (initMemeDisplayQueue, memesDir)
import Variadassist.WebPlayer.Paths (chromeSessionsDir, tempDir, viewerDataPath, webFilesDir)
import Variadassist.WebPlayer.Randomizer (randomizers)
import qualified Variadassist.WebPlayer.Randomizer as Randomizer
import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Types.State (State(..), ViewerData(..))
import Variadassist.WebPlayer.Web.Auth (withChatAuth, withStreamAuth)
import Variadassist.WebPlayer.Web.ControlPanel (err, message)
import Variadassist.WebPlayer.Web.EventSub (initEventSub)
import Variadassist.WebPlayer.Web.Ports (controlPanelPort, memesDirPort, overlaysPort, webFilesPort)
import Variadassist.WebPlayer.Web.PubSub (initPubSub)
import Variadassist.WebPlayer.Web.Utils (serveOne)

import Data.List.Extra (dropSuffix)
import qualified Data.Map as Map
import System.Directory (createDirectory, doesDirectoryExist, doesFileExist, removeDirectoryRecursive, removeFile)
import System.FilePath ((</>))
import System.Process (proc, shell)

readCommandFromPipe :: IOFallible String
readCommandFromPipe = do
	commandPipeReadProcess <- spawn "read from pipe" $ shell $
		"read line < \"" ++ commandPipePath ++ "\" && echo $line"
	tryIO $ statefully $ \state -> state{ commandPipeReadProcess = Just commandPipeReadProcess }
	command <- await commandPipeReadProcess
	return $ dropSuffix "\n" command.stdout

readAndExecCommands :: VoidFallible
readAndExecCommands = do
	message "Awaiting next command..."
	commandLine <- readCommandFromPipe
	message $ "Received command: " ++ msg commandLine

	if null commandLine then
		message "Empty command received; exiting..."
	else do
		debugM "Executing command in new thread..."
		case readPipeCommand commandLine of
			Error error ->
				err $ "The pipe command " ++ msg commandLine ++ " was not recognized: " ++ error
			Falue pipeCommand -> executeCommand pipeCommand
		readAndExecCommands

closeProgram :: VoidFallible
closeProgram = do
	hasState <- tryIO stateIsPresent
	if hasState then do
		let printAndIgnoreError operation = do
			voidOrError <- holdFal operation
			case voidOrError of
				Error error -> tryIO $ Messaging.err error
				Falue _ -> success

		leaveChat

		State{..} <- tryIO getState

		message "Saving subscriber data..."
		tryIO $ writeHaskellDataFile viewerDataPath viewerData

		tryIO $ Messaging.message "Closing sockets..."
		{- The control panel socket is already closed here; I don't know why this is the case, but
		   the reason may have something to do with HATs. -}
		maybe success (printAndIgnoreError . close) overlaySocket

		tryIO $ Messaging.message "Killing control panel..."
		printAndIgnoreError $ kill "the program is closing" controlPanelChromeProcess

		tryIO $ Messaging.message "Killing command pipe read process..."
		maybe success (printAndIgnoreError . kill "the program is closing") commandPipeReadProcess
	else success

	tempDirExists <- tryIO $ doesDirectoryExist tempDir
	if tempDirExists then do
		tryIO $ Messaging.message "Deleting temp dir..."
		tryIO $ removeDirectoryRecursive tempDir
	else success

initWebPlayer :: String -> String -> HATProcess -> WebSocket -> VoidFallible
initWebPlayer chatAuthToken streamAuthToken controlPanelChromeProcess controlPanelSocket = do
	haltableAsync "serve overlay" $ serveOne "overlay" "127.0.0.1" overlaysPort
		(\overlaySocket state -> state{ overlaySocket = Just overlaySocket })
		(const success)

	debugM "Initializing state..."
	memeDisplayQueue <- tryIO initMemeDisplayQueue

	(viewerData, maybeViewerDataError) <- tryIO $
		readHaskellDataFileOr viewerDataPath $ return ViewerData { intros = Map.empty }
	case maybeViewerDataError of
		Nothing -> debugM $ "Subscriber data loaded: " ++ show viewerData
		Just viewerDataError ->
			tryIO $ Messaging.warn $ "Subscriber data failed to load: " ++ viewerDataError

	tryIO $ initState State{
		controlPanelSocket = controlPanelSocket,
		overlaySocket = Nothing,
		chatAuthToken = chatAuthToken,
		streamAuthToken = streamAuthToken,
		streamTitle = Nothing,
		headphonesSink = Nothing,
		commandPipeReadProcess = Nothing,
		controlPanelChromeProcess = controlPanelChromeProcess,
		maybeMusicTask = Nothing,
		maybeCalculatorProcess = Nothing,
		memePreviewQueue = newQueue,
		memeDisplayQueue = memeDisplayQueue,
		textToSpeechPreviewQueue = [],
		textToSpeechPlayQueue = [],
		overlayAlertsQueue = newQueue,
		goals = [],
		timers = [],
		timerPrototypes = [],
		macroBoardMode = defaultMacroBoardMode,
		viewerData = viewerData }

	initEventSub
	initPubSub
	initChat

	captureMacros
	displayMode defaultMacroBoardMode

	readAndExecCommands

main = doFallibleMain $ do
	debugM "Starting program..."

	tryIO nohup
	tryIO $ onInterruptSignal closeProgram

	tempDirExists <- tryIO $ doesDirectoryExist tempDir
	if tempDirExists then do
		debugM "Clearing previous temp data..."
		tryIO $ removeDirectoryRecursive tempDir
	else success
	debugM "Building temp dirs..."
	tryIO $ createDirectory tempDir
	tryIO $ createDirectory memesDir

	debugM "Opening command pipe..."
	fileExists <- tryIO $ doesFileExist commandPipePath
	if fileExists
		then tryIO $ removeFile commandPipePath
		else success

	exec "create the command pipe" $ proc "mkfifo" [commandPipePath]
	message "Command pipe open!"

	serveDirHTTP memesDir memesDirPort
	{- Pain-in-the-ass Chrome won't let me load the styles file locally from a served control panel file,
	   so I'm serving styles separately to be loaded dynamically. -}
	serveDirHTTP webFilesDir webFilesPort
	tryIO $ mapM_ Randomizer.load randomizers

	withChatAuth \chatAuthToken -> withStreamAuth \streamAuthToken -> do
		-- TODO: rearrange to start the server first, then Chrome using some async as well as
		-- allow a new incoming control panel socket connection to replace the old one.
		chromeProcess <- spawn "control panel" $ proc "google-chrome" [
			"--new-window",
			"--allow-file-access-from-files",
			"--user-data-dir=" ++ chromeSessionsDir </> streamerUsername,
			"--app=http://localhost:" ++ show webFilesPort ++ "/control%20panel.html" ]

		tryIO $ Messaging.message "Awaiting control panel connection..."
		serveOne "control panel" "127.0.0.1" controlPanelPort
			(\socket state -> state{ controlPanelSocket = socket }) \controlPanelSocket ->
				initWebPlayer chatAuthToken streamAuthToken chromeProcess controlPanelSocket
