module Variadassist.WebPlayer.Stream (
	markStream
) where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.JSON (arraysOnly, extractValue, objectsOnly, numbersOnly, stringsOnly)
import Utils.Messaging (debugM)

import Variadassist.WebPlayer.Paths (markersDir)
import Variadassist.WebPlayer.Secrets (streamerUserID)
import Variadassist.WebPlayer.State (getState, statefully)
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Web.Helix (makeStreamMarker, fetchHelix)
import Variadassist.WebPlayer.Utils (showDuration)

import Control.Monad ((>=>))
import qualified Data.Text.Lazy as Text
import System.FilePath ((</>))

getStreamTitle :: IOFallible (Maybe String)
getStreamTitle = do
	State{..} <- tryIO getState
	case streamTitle of
		Just streamTitle -> return $ Just streamTitle
		Nothing -> do
			liveStreamResponse <- fetchHelix "live stream info" ["streams"]
				[("user_id", streamerUserID), ("type", "live")]
			liveStreamsInfo <- falT $ (
				objectsOnly >=>
				extractValue "data" >=>
				arraysOnly) liveStreamResponse
			case liveStreamsInfo of
				[] -> do
					debugM "This channel is not currently live on Twitch!"
					return Nothing
				[liveStreamInfo] -> do
					streamTitle <- fmap Text.unpack $ falT $
						(objectsOnly >=> extractValue "title" >=> stringsOnly) liveStreamInfo
					debugM $ "Live stream title found: " ++ show streamTitle
					tryIO $ statefully \state -> state{ streamTitle = Just streamTitle }
					return $ Just streamTitle
				multipleStreamsInfo ->
					errorT $ "Twitch says there are " ++
						show (length multipleStreamsInfo) ++
						" streams on this channel! How is that possible?!"

markStream :: String -> VoidFallible
markStream description = getStreamTitle >>= \case
	Nothing -> errorT $ "The stream doesn't seem to be running, so I can't make a mark in it!"
	Just streamTitle -> do
		helixResponse <- makeStreamMarker description
		markerTimeSec <- falT $ (objectsOnly >=>
			extractValue "data" >=>
			arraysOnly >=>
			(\array -> if length array == 1 then return (head array) else Error $
				"Twitch responded with " ++ show (length array) ++
					" markers when I tried to create one!") >=>
			objectsOnly >=>
			extractValue "position_seconds" >=>
			numbersOnly) helixResponse

		let markersFilePath = markersDir </> show streamTitle ++ ".txt"
		-- `appendFile` will create the file if it doesn't exist.
		tryIO $ appendFile markersFilePath $ (showDuration markerTimeSec) ++ "  " ++ description