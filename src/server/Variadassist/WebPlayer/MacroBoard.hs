module Variadassist.WebPlayer.MacroBoard (
	captureMacros,
	displayMode,
	setMacroBoardMode,
	defaultMacroBoardMode
) where

import Utils.Choose (requestInput)
import Utils.Fallible
import Utils.Fallible.Trans
import Utils.HATs
import Utils.Messaging hiding (err, message, warn)
import qualified Utils.Messaging as Messaging
import Utils.Processes hiding (message)
import Utils.Tuples
import Utils.WebSockets

import Variadassist.WebPlayer.Goals (failGoal, meetGoal, setGoal)
import Variadassist.WebPlayer.Memes (acceptNextMeme, discardNextMeme, discardNextMemeWithoutRefund, displayCopiedMeme)
import Variadassist.WebPlayer.Music (playMusicCategory, stopMusic)
import Variadassist.WebPlayer.OverlayAlerts (enqueueOverlayAlertByName)
import Variadassist.WebPlayer.Randomizer (randomizers)
import qualified Variadassist.WebPlayer.Randomizer as Randomizer
import Variadassist.WebPlayer.TextToSpeech (acceptNextTextToSpeech, discardNextTextToSpeech)
import Variadassist.WebPlayer.Types.Goals (Goal(..))
import Variadassist.WebPlayer.Types.MacroBoard
import Variadassist.WebPlayer.Types.Music (MusicCategory(..))
import Variadassist.WebPlayer.Types.Randomizer (Randomizer(..))
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Stream (markStream)
import Variadassist.WebPlayer.Utils (withChoiceFromStateAs)
import Variadassist.WebPlayer.Web.ControlPanel (err, message)
import Variadassist.WebPlayer.Web.Types.ControlPanel (MessageToControlPanel(..))
import Variadassist.WebPlayer.Web.Types.Timers (WebTimer(..), WebTimerPrototype)
import Variadassist.WebPlayer.Web.Timers (cancelTimer, nameTimerPrototype, newTimer, restartTimer, startTimer)

import Control.Monad (filterM)
import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as BS
import Data.Map ((!?))
import qualified Data.Map as Map
import Evdev (Device, Event(..), EventData(..), KeyEvent(..), deviceName, devicePath, evdevDir, grabDevice, newDevice, nextEvent)
import Evdev.Codes (Key(..))
import RawFilePath.Directory (listDirectory)
import System.FilePath.Posix.ByteString ((</>))
import System.Process (proc)

-- private utils

musicMode :: Bool -> MacroBoardMode
musicMode starting = do
	let play category subcategory = playMusicCategory MusicCategory {
			musicCategory = category,
			musicSubcategory = subcategory,
			musicStarting = starting }

	let playCategorySolo category = Perform category $ play category Nothing

	let subcategoryMode category subcategories = do
		let subToBinding subcategory = Perform subcategory $ play category $ Just subcategory
		let bindings = Map.insert
				KeySpace (Perform "no subcategory" $ play category Nothing) $
			Map.fromList $ map (mapSnd2 subToBinding) subcategories
		SwitchMode category $ MacroBoardMode ("select " ++ category ++ " subcategory") bindings

	MacroBoardMode "select music category" $ Map.fromList [
		(Key5, playCategorySolo "progression"),
		(KeyB, playCategorySolo "boss fight"),
		(KeyF, playCategorySolo "fight"),
		(KeyT, playCategorySolo "meetups"),
		(KeyBackspace, playCategorySolo "night"),
		(KeyG, playCategorySolo "garage"),
		(KeyD, subcategoryMode "day" [
			(Key1, "wind"),
			(KeyW, "water") ]),
		(KeyE, subcategoryMode "exploration" [
			(KeyS, "snow") ]),
		(KeyJ, subcategoryMode "looting" [
			(KeyE, "End"),
			(KeyH, "Nether") ]) ]
		

overlayAlertMode :: MacroBoardMode
overlayAlertMode = MacroBoardMode "select Overlay Alert" $ Map.fromList $
	map (mapSnd2 alertByName) [
		(Key1, "One More Thing"),
		(Key3, "Illuminati"),
		(KeyE, "Excellent"),
		(KeyG, "Vlad \"Now GOOOOO!\""),
		(KeyJ, "JJBA To Be Continued"),
		(KeyT, "Technoblade bruh"),
		(KeyV, "Villager sigh") ]
	where alertByName name = Perform name $ enqueueOverlayAlertByName name Nothing

spinRandomizerMode :: MacroBoardMode
spinRandomizerMode = MacroBoardMode "spin randomizer" $ Map.fromList $ map toSpinMode randomizers
	where
		toSpinMode r = (randomizerKey r, Perform (randomizerName r) $ Randomizer.spin r)

altMode :: MacroBoardMode
altMode = MacroBoardMode "ALT mode" $ Map.fromList [
	(KeyF1, SwitchMode "music with START" $ musicMode True),
	(KeyF11, Perform "show my meme" displayCopiedMeme),
	(KeyR, SwitchMode "spin randomizer" spinRandomizerMode)]

ctrlShiftMode :: MacroBoardMode
ctrlShiftMode = MacroBoardMode "CTRL SHIFT mode" $ Map.fromList [
	(KeyF10, Perform "discard next text to speech without refund" $ discardNextTextToSpeech False),
	(KeyF11, Perform "discard next meme without refund" discardNextMemeWithoutRefund) ]

ctrlMode :: MacroBoardMode
ctrlMode = MacroBoardMode "CTRL mode" $ Map.fromList [
	(KeyLeftshift, SwitchMode "-> discard without refund" ctrlShiftMode) ]

shiftMode :: MacroBoardMode
shiftMode = MacroBoardMode "SHIFT mode" $ Map.fromList [
	(KeyF10, Perform "discard next text to speech" $ discardNextTextToSpeech True),
	(KeyF11, Perform "discard next meme" discardNextMeme),
	(KeyLeftctrl, SwitchMode "-> discard without refund" ctrlShiftMode) ]

inputTimerProtoName :: WebTimerPrototype -> VoidFallible
inputTimerProtoName (protoName, _) = do
	maybeDisplayName <- requestInput "timer name"
	case maybeDisplayName of
		Nothing -> message $ "Naming of timer " ++ msg protoName ++ " cancelled!"
		Just displayName -> nameTimerPrototype protoName displayName

withChosenTimer :: ((WebTimer -> VoidFallible) -> VoidFallible)
withChosenTimer = withChoiceFromStateAs timerName timers "timers"

makeTimer :: VoidFallible
makeTimer = do
	maybeTimerName <- requestInput "timer name"
	case maybeTimerName of
		Nothing -> debugM "No timer name was input; cancelling make timer inquiry..."
		Just timerName -> do
			maybeTimerDuration <- requestInput "timer duration"
			case maybeTimerDuration of
				Nothing -> debugM "No timer duration was input; cancelling make timer inquiry..."
				Just timerDurationString -> do
					timerDuration <- tryIO $ readIO timerDurationString :: IOFallible Float
					newTimer timerName timerName timerDuration
					success

timerMode :: MacroBoardMode
timerMode = MacroBoardMode "configure timer" $ Map.fromList [
	(KeyC, Perform "name prototype" $
		withChoiceFromStateAs fst timerPrototypes "timer prototypes" inputTimerProtoName),
	(KeyT, Perform "make timer" makeTimer),
	(KeySpace, Perform "start timer" $ withChosenTimer startTimer),
	(KeyR, Perform "restart timer" $ withChosenTimer restartTimer),
	(KeyBackspace, Perform "cancel timer" $ withChosenTimer cancelTimer) ]

inputGoal :: VoidFallible
inputGoal = do
	maybeGoalName <- requestInput "goal"
	case maybeGoalName of
		Nothing -> debugM "No goal was input; cancelling goal setting..."
		Just goalName -> setGoal Goal {
			gName = goalName,
			gDisplayName = goalName,
			gOnMeet = success,
			gOnFail = success,
			gMarked = True }

withChosenGoal :: ((Goal -> VoidFallible) -> VoidFallible)
withChosenGoal = withChoiceFromStateAs gName goals "goals"

goalMode :: MacroBoardMode
goalMode = MacroBoardMode "configure goals" $ Map.fromList [
	(KeyG, Perform "set goal" inputGoal),
	(KeySpace, Perform "meet goal" $ withChosenGoal meetGoal),
	(KeyBackspace, Perform "fail goal" $ withChosenGoal failGoal) ]

displayRandomizerMode :: MacroBoardMode
displayRandomizerMode = MacroBoardMode "display randomizer" $ Map.fromList $ map toShowMode randomizers
	where
		toShowMode r = (randomizerKey r, Perform (randomizerName r) $ Randomizer.display r)

obeyKeyPress :: Key -> VoidFallible
obeyKeyPress key = do
	State{..} <- tryIO getState
	let (MacroBoardMode _ bindings) = macroBoardMode
	case bindings !? key of
		Nothing -> debugM $ "No action needed for " ++ show key ++ "; ignoring..."
		Just SwitchMode{..} -> setMacroBoardMode kpaNewMode
		Just Perform{..} -> do
			message $ "Executing " ++ msg kpaName ++ " macro board action..."
			haltableAsync ("macro board action " ++ show kpaName) kpaAction
			setMacroBoardMode defaultMacroBoardMode
			

obeyEvents :: Device -> VoidFallible
obeyEvents device = do
	State{..} <- tryIO getState
	let (MacroBoardMode modeName _) = macroBoardMode
	event <- tryIO $ nextEvent device
	case eventData event of
		KeyEvent key Pressed -> do
			result <- holdFal $ obeyKeyPress key
			case result of
				Error error -> err $ "An error occurred while attempting to obey a macro board key press:\n" ++ error ++ "\nMode: " ++ modeName ++ "; key: " ++ msg key
				Falue _ -> success
		_ -> success
	obeyEvents device

listenForEvents :: Device -> VoidFallible
listenForEvents device = do
	name <- tryIO $ fmap BS.toString $ deviceName device
	let path = BS.toString $ devicePath device
	debugM $ "Grabbing " ++ msg name ++ " at " ++ msg path ++ "..."
	tryIO $ grabDevice device
	haltableAsync ("listen to " ++ name ++ " at " ++ path) $ obeyEvents device
	success

toggleCalculator :: VoidFallible
toggleCalculator = do
	State{..} <- tryIO getState

	let spawnCalculator = do
		calculatorProcess <- spawn "calculator" $ proc "gnome-calculator" []
		tryIO $ statefully $ \state -> state{ maybeCalculatorProcess = Just calculatorProcess }

	case maybeCalculatorProcess of
		Nothing -> do
			debugM "No calculator process held; spawning calculator..."
			spawnCalculator
		Just calculatorProcess -> do
			running <- tryIO $ isRunning calculatorProcess
			if running then do
				debugM "Calculator process is running; killing..."
				kill "toggled via macro board" calculatorProcess
			else do
				debugM "Calculator process found, but not running; spawning calculator..."
				spawnCalculator

toggleMic :: VoidFallible
toggleMic = do
	spawn "toggle mic" $ proc "pactl" ["set-source-mute", "@DEFAULT_SOURCE@", "toggle"]
	success

-- exported utils

displayMode :: MacroBoardMode -> VoidFallible
displayMode mode@(MacroBoardMode name _) = do
	tryIO $ Messaging.message $ "Macro board mode: " ++ name
	State{..} <- tryIO $ getState
	sendJSON controlPanelSocket $ MacroBoardModeWebMessage mode

defaultMacroBoardMode :: MacroBoardMode
defaultMacroBoardMode = MacroBoardMode "default" $ Map.fromList [
	(KeyF1, SwitchMode "play music category" $ musicMode False),
	(KeyF4, Perform "toggle mic" toggleMic),
	(KeyF5, Perform "stop music" stopMusic),
	(KeyF9, SwitchMode "play Overlay Alert" overlayAlertMode),
	(KeyF10, Perform "accept next text to speech" acceptNextTextToSpeech),
	(KeyF11, Perform "accept next meme" acceptNextMeme),
	(KeyF12, Perform "toggle calculator" toggleCalculator),
	(KeyR, SwitchMode "show randomizer" displayRandomizerMode),
	(KeyT, SwitchMode "control timer" timerMode),
	(KeyG, SwitchMode "control goals" goalMode),
	(KeyLeftalt, SwitchMode "ALT" altMode),
	(KeyLeftshift, SwitchMode "SHIFT" shiftMode),
	(KeyLeftctrl, SwitchMode "CTRL" ctrlMode),
	(KeyEnter, Perform "mark" $ markStream "manual") ]

setMacroBoardMode :: MacroBoardMode -> VoidFallible
setMacroBoardMode mode@(MacroBoardMode modeName modeBindings) = do
	displayMode mode
	let bindingsWithCommon = 
		Map.insert KeyEsc (Perform "return to default mode" success) $
			modeBindings
	tryIO $ statefully $ \state -> state{ macroBoardMode = MacroBoardMode modeName bindingsWithCommon }

captureMacros :: VoidFallible
captureMacros = do
	debugM "Seeking macro board..."
	devicePaths <- tryIO $ listDirectory evdevDir
	eventDevices <- tryIO $ mapM (newDevice . (evdevDir </>)) $
		filter (BS.fromString "event" `BS.isPrefixOf`) devicePaths
	debugM $ show (length eventDevices) ++ " event devices detected: " ++
		msgList (map (BS.toString . devicePath) eventDevices) ++ "; seeking gaming keyboard..."
	let isGamingKeyboard = fmap (BS.fromString "Gaming Keyboard" `BS.isInfixOf`) . deviceName
	macroBoardDevices <- tryIO $ filterM isGamingKeyboard eventDevices
	mapM_ listenForEvents macroBoardDevices
