module Variadassist.WebPlayer.TextToSpeech (
	textToSpeechStartDelaySec,
	textToSpeechEndBufferSec,
	previewTextToSpeech,
	playTextToSpeech,
	acceptNextTextToSpeech,
	discardNextTextToSpeech
) where

import Utils.Fallible.Trans
import Utils.HATs
import Utils.Messaging hiding (err, message, warn)
import Utils.Processes hiding (message)
import Utils.Queues
import Utils.WebSockets

import Control.Concurrent (threadDelay)
import Data.Char (toLower)
import System.Process (proc)
import Variadassist.WebPlayer.State (getState, statefully, statefullyOutputting)
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Web.ControlPanel (message)
import Variadassist.WebPlayer.Web.Helix (fulfillRedemption, rejectRedemption)
import Variadassist.WebPlayer.Web.Types.ControlPanel (MessageToControlPanel(DequeueTextToSpeech, PreviewTextToSpeech))
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(ShowTextToSpeech, TextToSpeechFinished))
import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption(..))
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

-- constants

textToSpeechStartDelaySec = 0.75

textToSpeechEndBufferSec = 1.5

-- private utils

dequeueNextTextToSpeechPreview :: IOFallible (String, RewardRedemption)
dequeueNextTextToSpeechPreview = do
	debugM $ "Dequeueing next text to speech message..."
	maybeDequeuedTTS <- tryIO $ statefullyOutputting $ \state@State{..} ->
		let (maybeDequeuedTTS, newPreviewQueue) = dequeue textToSpeechPreviewQueue
		in (state{ textToSpeechPreviewQueue = newPreviewQueue }, maybeDequeuedTTS)

	case maybeDequeuedTTS of
		Nothing -> errorT "There was no text to speech to dequeue!"
		Just dequeuedTTS -> do
			State{..} <- tryIO getState
			sendJSON controlPanelSocket DequeueTextToSpeech
			return dequeuedTTS

playTTSQueue :: VoidFallible
playTTSQueue = do
	State{..} <- tryIO getState
	let (maybeDequeuedTTS, _) = dequeue textToSpeechPlayQueue
	case maybeDequeuedTTS of
		Nothing -> success
		Just (ttsMessage, ttsRR@RewardRedemption{..}) -> do
			sendJSONToPanels $ ShowTextToSpeech ttsMessage rrUser
			tryIO $ threadDelay $ floor (textToSpeechStartDelaySec * 1000000)
			playTextToSpeech ttsMessage
			fulfillRedemption ttsRR
			sendJSONToPanels TextToSpeechFinished
			tryIO $ threadDelay $ floor (textToSpeechEndBufferSec * 100000)
			playQueueIsEmpty <- tryIO $ statefullyOutputting $ \state@State{..} ->
				let (_, newPlayQueue) = dequeue textToSpeechPlayQueue
				in (state{ textToSpeechPlayQueue = newPlayQueue }, null newPlayQueue)
			if playQueueIsEmpty
				then success
				else playTTSQueue

-- exported utils

playTextToSpeech :: String -> VoidFallible
playTextToSpeech message = do
	exec ("play text to speech" ++ msg message) $ proc "text-to-speech" [map toLower message]
	success

previewTextToSpeech :: String -> RewardRedemption -> VoidFallible
previewTextToSpeech message rr@RewardRedemption{..} = do
	debugM $ "Enqueueing text to speech message " ++ msg message ++
		" from " ++ rrUser ++ "..."
	tryIO $ statefully $ \state@State{..} ->
		state{ textToSpeechPreviewQueue = enqueue textToSpeechPreviewQueue (message, rr) }

	State{..} <- tryIO getState
	sendJSON controlPanelSocket $ PreviewTextToSpeech message rrUser

acceptNextTextToSpeech :: VoidFallible
acceptNextTextToSpeech = do
	message "Accepting next text to speech..."
	tts <- dequeueNextTextToSpeechPreview
	playQueueWasEmpty <- tryIO $ statefullyOutputting $ \state@State{..} ->
		(state{ textToSpeechPlayQueue = enqueue textToSpeechPlayQueue tts }, null textToSpeechPlayQueue)
	if playQueueWasEmpty
		then haltableAsync "playing text to speech queue" playTTSQueue >> success
		else success

discardNextTextToSpeech :: Bool -> VoidFallible
discardNextTextToSpeech refundPoints = do
	message "Discarding next text to speech..."
	(_, ttsRR) <- dequeueNextTextToSpeechPreview
	if refundPoints
		then rejectRedemption ttsRR
		else fulfillRedemption ttsRR
