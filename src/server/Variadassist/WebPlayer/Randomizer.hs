module Variadassist.WebPlayer.Randomizer (
	randomizers,
	choose,
	display,
	load,
	save,
	spin
) where

import Utils.Fallible.Trans
import Utils.Messaging (debugM, msg)
import Utils.Regex ((*//~), res)

import Variadassist.WebPlayer.Types.Randomizer
import Variadassist.WebPlayer.Web.ControlPanel (message)
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(..))
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

import Data.IORef (atomicModifyIORef, newIORef, readIORef, writeIORef)
import Data.List (findIndex, intercalate)
import Evdev.Codes (Key(KeyW))
import Numeric.Extra (intToFloat)
import System.IO.Unsafe (unsafeDupablePerformIO)
import System.Random (randomRIO)

-- constants

randomizers :: [Randomizer]
randomizers = unsafeDupablePerformIO $ do
	-- Find a better way. This sucks.
	wmwWeightsRef <- newIORef $ defaultWeightsForCount 5
	return [
		WeightedAdjustingRandomizer {
			randomizerName = "Wild Mage Weekends",
			randomizerKey = KeyW,
			randomizerEntries = ["Transistor", "Terraria", "Nethack", "Scrap Mechanic", "Bioshock"],
			randomizerShowTitles = False,
			randomizerWeightsRef = wmwWeightsRef } ]

-- private utils

getWeights :: Randomizer -> IO [Weight]
getWeights randomizer@EvenRandomizer{..} = return $ defaultWeightsFor randomizer
getWeights WeightedAdjustingRandomizer{..} = readIORef randomizerWeightsRef

reweigh :: String -> Randomizer -> IO [Weight]
reweigh _ randomizer@EvenRandomizer{..} = return $ defaultWeightsFor randomizer
reweigh winner WeightedAdjustingRandomizer{..} = atomicModifyIORef randomizerWeightsRef $ \weights -> do
	debugM $ "Reweighing " ++ show weights ++ " for " ++ msg randomizerName ++ "..."
	let loserCount = length weights - 1
	let (Just winnerIndex) = findIndex (==winner) randomizerEntries
	let winnerWeight = weights !! winnerIndex
	let weightToRedistribute = min (100 / intToFloat loserCount) winnerWeight

	let reweighAt :: (Int, Weight) -> Weight = \(i, weight) ->
		if i == winnerIndex
			then max 0.0 $ weight - weightToRedistribute
			else weight + (weightToRedistribute / intToFloat loserCount)

	let newWeights :: [Weight] = map reweighAt $ zip [0..] weights
	debugM $ "Weights reweighed for " ++ msg randomizerName ++ ": " ++ show weights ++ "; storing..."
	(newWeights, newWeights)

-- exported utils

load :: Randomizer -> Void
load EvenRandomizer{..} = return ()
load randomizer@WeightedAdjustingRandomizer{..} = do
	let (Just filePath) = weightsFilePath randomizer  -- always present for WeightedAdjustingRandomizers
	weightsFileContent <- readFile filePath
	let weightsString = weightsFileContent *//~ [res|\{-.*?-\}///|]
	weights <- readIO weightsString

	debugM $ "Loaded weights " ++ show weights ++ " from " ++ filePath ++ "; storing..."
	writeIORef randomizerWeightsRef weights

save :: Randomizer -> Void
save EvenRandomizer{..} = return ()
save randomizer@WeightedAdjustingRandomizer{..} = do
	let (Just filePath) = weightsFilePath randomizer  -- always present for WeightedAdjustingRandomizers
	weights <- readIORef randomizerWeightsRef
	debugM $ "Saving weights " ++ show weights ++ " to " ++ filePath ++ "..."
	let entryAndWeightToString entry weight = "{- " ++ entry ++ " -} " ++ show weight
	let weightsString = intercalate ", " $ map (uncurry entryAndWeightToString) $ zip randomizerEntries weights
	writeFile filePath $ "[ " ++ weightsString ++ " ]"

display :: Randomizer -> VoidFallible
display randomizer = do
	weights <- tryIO $ getWeights randomizer
	sendJSONToPanels DisplayRandomizerWebMessage {
		drwmName = randomizerName randomizer,
		drwmEntries = randomizerEntries randomizer,
		drwmWeights = weights,
		drwmShowTitles = randomizerShowTitles randomizer }

choose :: Randomizer -> IO (String, Float)
choose randomizer = do
	weights <- getWeights randomizer
	spectrumValue <- randomRIO (0.0, 100.0) :: IO Float

	let findWinner :: Float -> [Weight] -> [String] -> String =
		\spectrumToTraverse (weight:weightsRemaining) (entry:entriesRemaining) ->
			if null weightsRemaining || spectrumToTraverse <= weight
				then entry
				else findWinner (spectrumToTraverse - weight) weightsRemaining entriesRemaining
	let winner = findWinner spectrumValue weights $ randomizerEntries randomizer

	return (winner, spectrumValue)

spin :: Randomizer -> VoidFallible
spin randomizer = do
	(winner, spectrumValue) <- tryIO $ choose randomizer

	message $ "Winner chosen on " ++ randomizerName randomizer ++ ": " ++
		show winner ++ " (" ++ show spectrumValue ++ "/100)!"

	oldWeights <- tryIO $ getWeights randomizer
	newWeights <- tryIO $ reweigh winner randomizer
	tryIO $ save randomizer

	sendJSONToPanels SpinRandomizerWebMessage {
		srwmName = randomizerName randomizer,
		srwmEntries = randomizerEntries randomizer,
		srwmWeights = oldWeights,
		srwbWinner = winner,
		srwmSpectrumValue = spectrumValue,
		srwmNewWeights = newWeights,
		srwmShowTitles = randomizerShowTitles randomizer }
