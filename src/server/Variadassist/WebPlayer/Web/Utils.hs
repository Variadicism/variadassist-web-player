module Variadassist.WebPlayer.Web.Utils (
	sendJSONToPanels,
	serveOne
) where

import Utils.Fallible.Trans
import Utils.Messaging (debugM)
import Utils.WebSockets (WebSocket, sendJSON, serveAsync)

import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Types.State (State(..))

import Data.Aeson (ToJSON, toJSON)
import Data.Maybe (catMaybes)

sendJSONToPanels :: ToJSON j => j -> VoidFallible
sendJSONToPanels json = do
	debugM $ "Messaging all panels: " ++ show (toJSON json)
	State{..} <- tryIO getState
	tryIO $ mapM_ (elseErr . flip sendJSON json) $ catMaybes [Just controlPanelSocket, overlaySocket]

-- TODO: try Lenses to simplify setState to one state field to overwrite.
serveOne :: String -> String -> Int -> (WebSocket -> State -> State) ->
	(WebSocket -> VoidFallible) -> VoidFallible
serveOne name host port setState onReconnect =
	serveAsync name host port (\_ -> return $ name ++ " socket") \socket _ -> do
		initialized <- tryIO stateIsPresent
		if initialized
			then tryIO $ statefully $ setState socket
			else onReconnect socket
		
