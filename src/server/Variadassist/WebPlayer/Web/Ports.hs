module Variadassist.WebPlayer.Web.Ports (
	overlaysPort,
	controlPanelPort,
	webFilesPort,
	memesDirPort
) where

overlaysPort :: Int
overlaysPort = 7041

controlPanelPort :: Int
controlPanelPort = 7042

webFilesPort :: Int
webFilesPort = 7043

memesDirPort :: Int
memesDirPort = 7044
