module Variadassist.WebPlayer.Web.Timers (
	newTimerPrototype,
	nameTimerPrototype,
	newTimer,
	newSilentTimer,
	newOrRestartTimer,
	startTimer,
	cancelTimer,
	restartTimer
) where

import Utils.Fallible.Trans
import Utils.Messaging

import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(..))
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Utils (playSound)
import Variadassist.WebPlayer.Web.Types.Timers
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

import Control.Concurrent.Suspend (msDelay)
import Control.Monad (when)
import qualified Control.Concurrent.Timer as Timer
import Data.List (find)

-- constants

clockTickingVolume = 15

clockBellVolume = 60

-- private utils

deleteTimer :: WebTimer -> Void
deleteTimer timer = statefully $ \state@State{..} -> state{ timers = filter (/= timer) timers }

createAndSaveTimer :: String -> String -> Float -> Bool ->  IO WebTimer
createAndSaveTimer name displayName duration silent = do
	timer <- Timer.newTimer
	let webTimer = WebTimer {
		timerName = name,
		timerDisplayName = displayName,
		timerDurationSeconds = duration,
		timerSilent = silent,
		timerTimer = timer }
	debugM $ "New timer " ++ show webTimer ++ " created! Saving..."
	statefully $ \state@State{..} -> state{ timers = webTimer:timers }
	return webTimer

newTimerWithSilence :: Bool -> String -> String -> Float -> IOFallible WebTimer
newTimerWithSilence silent name displayName duration = do
	webTimer <- tryIO $ createAndSaveTimer name displayName duration silent
	sendJSONToPanels $ TimerMakeWebMessage webTimer
	return webTimer

-- exported utils

newTimerPrototype :: String -> Float -> VoidFallible
newTimerPrototype name duration = do
	debugM $ "Creating timer prototype " ++ msg name ++ " for " ++
		show duration ++ " seconds..."
	sendJSONToPanels $ TimerPrototypeWebMessage name duration
	tryIO $ statefully $ \state@State{..} -> state{ timerPrototypes = (name, duration):timerPrototypes }

nameTimerPrototype :: String -> String -> VoidFallible
nameTimerPrototype name displayName = do
	State{..} <- tryIO getState
	let maybeTimerProto = find ((name==) . fst) timerPrototypes
	case maybeTimerProto of
		Nothing -> errorT $ "No timer prototype named " ++ msg name ++ " was found!"
		Just (_, duration) -> do
			tryIO $ statefully $ \state@State{..} -> state{ timerPrototypes = filter ((name/=) . fst) timerPrototypes }
			-- TODO: in the future, allow timer prototypes to be silent.
			tryIO $ createAndSaveTimer name displayName duration False
			sendJSONToPanels $ TimerNameWebMessage name displayName
			success

newTimer :: (String -> String -> Float -> IOFallible WebTimer)
newTimer = newTimerWithSilence False

newSilentTimer :: (String -> String -> Float -> IOFallible WebTimer)
newSilentTimer = newTimerWithSilence True

newOrRestartTimer :: String -> Float -> IOFallible WebTimer
newOrRestartTimer name delay = do
	State{..} <- tryIO getState
	case find ((name==) . timerName) timers of
		Nothing -> do
			debugM $ "No timer named " ++ msg name ++ " was found; making new timer..."
			newTimer name name delay
		Just timer -> do
			debugM $ "A timer already exists named " ++ msg name ++ "; restarting..."
			restartTimer timer
			return timer

startTimer :: WebTimer -> VoidFallible
startTimer timer@WebTimer{..} = do
	let onTimerComplete = do
		debugM $ "Timer " ++ msg timer ++ " completed! Deleting..."
		falM $ when (not timerSilent) $ playSound clockBellSoundPath clockBellVolume
		deleteTimer timer
	result <- tryIO $ Timer.oneShotStart timerTimer onTimerComplete
		(msDelay $ fromIntegral $ floor timerDurationSeconds * 1000)
	if result then do
		debugM $ "Timer " ++ msg timer ++ " was started. Completion in " ++ show timerDurationSeconds ++ " seconds!"
		sendJSONToPanels $ TimerStartWebMessage timerName
		when (not timerSilent) $ playSound clockTickingSoundPath clockTickingVolume
	else errorT $ "Starting timer " ++ msg timer ++ " failed!"

cancelTimer :: WebTimer -> VoidFallible
cancelTimer timer@WebTimer{..} = do
	tryIO $ Timer.stopTimer timerTimer
	debugM $ "Timer " ++ msg timer ++ " was cancelled."
	tryIO $ deleteTimer timer
	sendJSONToPanels $ TimerCancelWebMessage timerName
	when (not timerSilent) $ playSound clockTickingStopSoundPath clockTickingVolume

restartTimer :: WebTimer -> VoidFallible
restartTimer timer@WebTimer{..} = do
	result <- tryIO $ Timer.oneShotRestart timerTimer
	if result then do
		debugM $ "Timer " ++ msg timer ++ " was restarted."
		sendJSONToPanels $ TimerRestartWebMessage timerName
		when (not timerSilent) $ playSound clockBellReversedSoundPath clockBellVolume
	else errorT $ "Restarting timer " ++ msg timer ++ " failed!"
