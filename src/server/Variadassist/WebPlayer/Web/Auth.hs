module Variadassist.WebPlayer.Web.Auth (
	withChatAuth,
	withStreamAuth
) where

import Utils.Fallible.Trans

import Variadassist.Twitch.Auth (withTwitchAuth)
import Variadassist.Twitch.Secrets (botClientID, streamerClientID)

-- constants

streamAuthScopes = [
	"channel:read:redemptions",
	"channel:manage:redemptions",
	"channel:manage:broadcast",
	"moderator:read:followers",
	"channel:read:subscriptions" ]

chatAuthScopes = [
	"chat:read",
	"chat:edit",
	"channel:moderate",
	"moderator:manage:chat_messages" ]

-- exported utils

withChatAuth :: ((String -> VoidFallible) -> VoidFallible)
withChatAuth = withTwitchAuth "chat" "Variadassist" botClientID chatAuthScopes

withStreamAuth :: ((String -> VoidFallible) -> VoidFallible)
withStreamAuth = withTwitchAuth "stream" "Variadicism" streamerClientID streamAuthScopes
