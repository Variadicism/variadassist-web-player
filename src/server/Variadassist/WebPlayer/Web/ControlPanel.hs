module Variadassist.WebPlayer.Web.ControlPanel (
	message,
	warn,
	err,
	elseErr,
	elseWarn
) where

import Utils.Fallible
import Utils.Fallible.Trans hiding (elseErr)
import qualified Utils.Messaging as Messaging
import Utils.WebSockets (sendJSON)

import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.State (getState, stateIsPresent)
import Variadassist.WebPlayer.Web.Types.ControlPanel (MessageLevel(..), MessageToControlPanel(ServerMessageWebMessage))

-- private utils

sendControlPanelMessage :: MessageLevel -> (String -> Void) -> String -> VoidFallible
sendControlPanelMessage level messenger message = do
	tryIO $ messenger message

	hasState <- tryIO stateIsPresent
	if hasState then do
		State{..} <- tryIO getState
		sendJSON controlPanelSocket $ ServerMessageWebMessage level message
	else
		success

-- exported utils

message :: (String -> VoidFallible)
message = sendControlPanelMessage NormalMessage Messaging.message

warn :: (String -> VoidFallible)
warn = sendControlPanelMessage WarningMessage Messaging.warn

err :: (String -> VoidFallible)
err = sendControlPanelMessage ErrorMessage Messaging.err

elseErr :: VoidFallible -> VoidFallible
elseErr errorProneTask = do
	fal <- tryIO $ falM errorProneTask
	fallible err return fal

elseWarn :: VoidFallible -> VoidFallible
elseWarn errorProneTask = do
	fal <- tryIO $ falM errorProneTask
	fallible warn return fal