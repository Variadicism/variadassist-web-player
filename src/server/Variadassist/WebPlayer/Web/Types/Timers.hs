module Variadassist.WebPlayer.Web.Types.Timers (
	WebTimer(..),
	WebTimerPrototype,
	clockTickingSoundPath,
	clockTickingStopSoundPath,
	clockBellSoundPath,
	clockBellReversedSoundPath,
) where

import Utils.Messaging

import Variadassist.WebPlayer.Paths (soundEffectsDir)

import Control.Concurrent.Timer (TimerIO)
import System.FilePath ((</>))

-- constants

clockTickingSoundPath :: FilePath
clockTickingSoundPath = soundEffectsDir </> "clock ticking.mp3"

clockTickingStopSoundPath :: FilePath
clockTickingStopSoundPath = soundEffectsDir </> "clock ticking stop.mp3"

clockBellSoundPath :: FilePath
clockBellSoundPath = soundEffectsDir </> "clock bell.mp3"

clockBellReversedSoundPath :: FilePath
clockBellReversedSoundPath = soundEffectsDir </> "clock bell reversed.mp3"

-- types

data WebTimer = WebTimer {
	timerName :: String,
	timerDisplayName :: String,
	timerDurationSeconds :: Float,
	timerSilent :: Bool,
	timerTimer :: TimerIO }

type WebTimerPrototype = (String, Float)

-- instances

instance Eq WebTimer where
	(==) t1 t2 = timerName t1 == timerName t2

instance Messagable WebTimer where
	msg WebTimer{..} = msg timerName ++ " (" ++ msg timerDisplayName ++ ")"

instance Show WebTimer where
	show WebTimer{..} =
		timerName ++ "(" ++ timerDisplayName ++ ") @" ++ show timerDurationSeconds ++ "s"
