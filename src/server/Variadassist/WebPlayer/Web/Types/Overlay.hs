module Variadassist.WebPlayer.Web.Types.Overlay (
	MessageToOverlay(..)
) where

import Utils.JSON

import Variadassist.WebPlayer.Types.Goals (Goal(..))
import Variadassist.WebPlayer.Types.Randomizer (Weight)
import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption(..))
import Variadassist.WebPlayer.Web.Types.Timers (WebTimer(..))

import Data.Aeson ((.=), ToJSON, toJSON)
import qualified Data.Aeson as Aeson
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text

-- types

data MessageToOverlay =
	MusicCategoryWebMessage {
		cwmCategory :: String,
		cwmSubcategory :: Maybe String,
		cwmStarting :: Bool,
		cwmSongTitle :: String,
		cwmSongArtist :: String } |
	SongWebMessage {
		swmSongTitle :: String,
		swmSongArtist :: String } |
	StopMusicWebMessage |
	ShowMemeWebMessage String String |
	HideMemeWebMessage |
	ShowOverlayAlertWebMessage {
		soawmName :: String,
		soawmImageURL :: String } |
	DisplayRandomizerWebMessage {
		drwmName :: String,
		drwmEntries :: [String],
		drwmWeights :: [Weight],
		drwmShowTitles :: Bool } |
	SpinRandomizerWebMessage {
		srwmName :: String,
		srwmEntries :: [String],
		srwmWeights :: [Weight],
		srwbWinner :: String,
		srwmSpectrumValue :: Float,
		srwmNewWeights :: [Weight],
		srwmShowTitles :: Bool } |
	TimerPrototypeWebMessage String Float |
	TimerNameWebMessage String String |
	TimerMakeWebMessage WebTimer |
	TimerStartWebMessage String |
	TimerRestartWebMessage String |
	TimerCancelWebMessage String |
	ShowTextToSpeech String String |
	TextToSpeechFinished |
	SetGoalWebMessage Goal |
	MeetGoalWebMessage String |
	FailGoalWebMessage String |
	FollowWebMessage String |
	SubscribeWebMessage String |
	ChannelPointsRedemptionWebMessage RewardRedemption

-- instances

instance ToJSONObject MessageToOverlay where
	toJSONObject MusicCategoryWebMessage{..} = jsonObject [
		"action" .= ("music category" :: Text),
		"category" .= cwmCategory,
		"subcategory" .= cwmSubcategory,
		"starting" .= cwmStarting,
		"songTitle" .= cwmSongTitle,
		"songArtist" .= cwmSongArtist ]
	toJSONObject SongWebMessage{..} = jsonObject [
		"action" .= ("song" :: Text),
		"songTitle" .= swmSongTitle,
		"songArtist" .= swmSongArtist ]
	toJSONObject StopMusicWebMessage = jsonObject [ "action" .= ("stop music" :: Text) ]
	toJSONObject (ShowMemeWebMessage url user) = jsonObject [
		"action" .= ("meme" :: Text),
		"url" .= url,
		"user" .= user ]
	toJSONObject HideMemeWebMessage = jsonObject [ "action" .= ("hide meme" :: Text) ]
	toJSONObject ShowOverlayAlertWebMessage{..} = jsonObject [
		"action" .= ("overlayAlert" :: Text),
		"name" .= soawmName,
		"url" .= soawmImageURL ]
	toJSONObject DisplayRandomizerWebMessage{..} = jsonObject [
		"action" .= ("display randomizer" :: Text),
		"name" .= Text.pack drwmName,
		"entries" .= map Text.pack drwmEntries,
		"weights" .= drwmWeights,
		"showTitles" .= drwmShowTitles ]
	toJSONObject SpinRandomizerWebMessage{..} = jsonObject [
		"action" .= ("spin randomizer" :: Text),
		"name" .= Text.pack srwmName,
		"entries" .= srwmEntries,
		"winner" .= srwbWinner,
		"spectrumValue" .= srwmSpectrumValue,
		"weights" .= srwmWeights,
		"newWeights" .= srwmNewWeights,
		"showTitles" .= srwmShowTitles ]
	toJSONObject (TimerMakeWebMessage WebTimer{..}) = jsonObject [
		"action" .= ("make timer" :: Text),
		"name" .= Text.pack timerName,
		"displayName" .= Text.pack timerDisplayName,
		"duration" .= timerDurationSeconds ]
	toJSONObject (TimerPrototypeWebMessage name duration) = jsonObject [
		"action" .= ("prototype timer" :: Text),
		"name" .= Text.pack name,
		"duration" .= duration ]
	toJSONObject (TimerNameWebMessage name displayName) = jsonObject [
		"action" .= ("name prototype timer" :: Text),
		"name" .= name,
		"displayName" .= displayName ]
	toJSONObject (TimerStartWebMessage name) = jsonObject [
		"action" .= ("start timer" :: Text),
		"name" .= Text.pack name ]
	toJSONObject (TimerRestartWebMessage name) = jsonObject [
		"action" .= ("restart timer" :: Text),
		"name" .= Text.pack name ]
	toJSONObject (TimerCancelWebMessage name) = jsonObject [
		"action" .= ("cancel timer" :: Text),
		"name" .= Text.pack name ]
	toJSONObject (ShowTextToSpeech message user) = jsonObject [
		"action" .= ("show text to speech" :: Text),
		"message" .= Text.pack message,
		"user" .= Text.pack user ]
	toJSONObject TextToSpeechFinished = jsonObject [
		"action" .= ("text to speech finished" :: Text) ]
	toJSONObject (SetGoalWebMessage Goal{..}) = jsonObject [
		"action" .= ("set goal" :: Text),
		"goal" .= Text.pack gName,
		"display" .= Text.pack gDisplayName ]
	toJSONObject (MeetGoalWebMessage goalName) = jsonObject [
		"action" .= ("meet goal" :: Text),
		"goal" .= Text.pack goalName ]
	toJSONObject (FailGoalWebMessage goalName) = jsonObject [
		"action" .= ("fail goal" :: Text),
		"goal" .= Text.pack goalName ]
	toJSONObject (FollowWebMessage user) = jsonObject [
		"action" .= ("follow" :: Text),
		"user" .= Text.pack user ]
	toJSONObject (SubscribeWebMessage user) = jsonObject [
		"action" .= ("subscribe" :: Text),
		"user" .= Text.pack user ]
	toJSONObject (ChannelPointsRedemptionWebMessage RewardRedemption{..}) = jsonObject $ [
		"action" .= ("redeem Channel Points" :: Text),
		"user" .= Text.pack rrUser,
		"reward" .= Text.pack rrTitle,
		"priority" .= rrPriority ] ++
		maybe [] (\message -> ["message" .= Text.pack message]) rrUserInput

instance ToJSON MessageToOverlay where
	toJSON = Aeson.Object . toJSONObject
