module Variadassist.WebPlayer.Web.Types.ControlPanel (
	MessageToControlPanel(..),
	MessageLevel(..)
) where

import Utils.JSON
import Utils.Messaging

import Variadassist.WebPlayer.Types.Chat (IRCMessage, IRCMessageOrigin, toIRCString)
import Variadassist.WebPlayer.Types.MacroBoard (KeyPressAction(kpaName), MacroBoardMode(..))
import Variadassist.WebPlayer.Instances ()
import Variadassist.WebPlayer.Web.Types.PubSub (MessageToPubSub, SymmetricalPubSubMessage)

import Data.Aeson ((.=), ToJSON, toJSON)
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Key as Aeson
import qualified Data.Map as Map
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text

-- types

data MessageLevel = NormalMessage | WarningMessage | ErrorMessage

data MessageToControlPanel =
	SymPubSubWebMessage SymmetricalPubSubMessage |
	ToPubSubWebMessage MessageToPubSub |
	FromChatWebMessage (Maybe IRCMessageOrigin) String |
	ToIRCWebMessage IRCMessage |
	ErrorToDisplayMessage String |
	PreviewMemeWebMessage String String |
	DequeueMemePreviewMessage |
	ServerMessageWebMessage MessageLevel String |
	MacroBoardModeWebMessage MacroBoardMode |
	PreviewTextToSpeech String String |
	DequeueTextToSpeech

-- instances

instance Show MessageLevel where
	show NormalMessage = "normal"
	show WarningMessage = "warning"
	show ErrorMessage = "error"

instance Messagable MessageLevel where
	msg = show

instance ToJSONObject MessageToControlPanel where
	toJSONObject (SymPubSubWebMessage content) = jsonObject [
		"action" .= ("to PubSub" :: Text),
		"content" .= toJSON content ]
	toJSONObject (ToPubSubWebMessage content) = jsonObject [
		"action" .= ("to PubSub" :: Text),
		"content" .= toJSON content ]
	toJSONObject (FromChatWebMessage origin message) = jsonObject [
		"action" .= ("from chat" :: Text),
		"origin" .= Text.pack (maybe "UNKNOWN SENDER" show origin),
		"message" .= Text.pack message ]
	toJSONObject (ToIRCWebMessage ircMessage) = jsonObject [
		"action" .= ("to IRC" :: Text),
		"content" .= Text.pack (toIRCString ircMessage) ]
	toJSONObject (ErrorToDisplayMessage error) = jsonObject [
		"action" .= ("Variadassist error" :: Text),
		"error" .= error ]
	toJSONObject (PreviewMemeWebMessage url user) = jsonObject [
		"action" .= ("preview meme" :: Text),
		"url" .= url,
		"user" .= user ]
	toJSONObject DequeueMemePreviewMessage = jsonObject [
		"action" .= ("dequeue meme" :: Text) ]  -- TODO: change this to "dequeue meme preview"
	toJSONObject (ServerMessageWebMessage level message) = jsonObject [
		"action" .= ("server message" :: Text),
		"level" .= Text.pack (show level),
		"message" .= Text.pack message ]
	toJSONObject (MacroBoardModeWebMessage (MacroBoardMode name bindings)) = jsonObject [
		"action" .= ("macro board mode" :: Text),
		"name" .= Text.pack name,
		"bindings" .= (jsonObject $ map assocToJSONPair $ Map.assocs bindings) ]
		where assocToJSONPair (key, keyAction) =
			Aeson.fromString (msg key) .= Text.pack (kpaName keyAction)
	toJSONObject (PreviewTextToSpeech message user) = jsonObject [
		"action" .= ("preview text to speech" :: Text),
		"message" .= Text.pack message,
		"user" .= Text.pack user ]
	toJSONObject DequeueTextToSpeech = jsonObject [
		"action" .= ("dequeue text to speech" :: Text) ]

instance ToJSON MessageToControlPanel where
	toJSON = Aeson.Object . toJSONObject
