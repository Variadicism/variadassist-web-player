module Variadassist.WebPlayer.Web.Types.PubSub (
	MessageFromPubSub(..),
	MessageToPubSub(..),
	RewardRedemption(..),
	SymmetricalPubSubMessage(..)
) where

import Utils.Bytes
import Utils.JSON
import Utils.Lists
import Utils.Messaging

import Data.Aeson ((.=), ToJSON, toJSON)
import qualified Data.Aeson as Aeson
import Data.Maybe (isJust)
import Data.Text.Lazy (Text)

-- types

data MessageFromPubSub = ChannelPointsReward { cprmRewardName :: String }

data MessageToPubSub = Listen { lpsmEventTypes :: [String], lpsmAuthToken :: String }

data RewardRedemption = RewardRedemption {
	rrID :: String,
	rrRewardID :: String,
	rrTitle :: String,
	rrUser :: String,
	rrUserInput :: Maybe String,
	rrPriority :: Bool } deriving Show

data SymmetricalPubSubMessage = Ping (Maybe String) |
	Pong (Maybe String) |
	Reconnect

-- instances

instance ToJSONObject SymmetricalPubSubMessage where
	toJSONObject (Ping pingNonce) = jsonObject $ listIf [
		(True, "type" .= ("PING" :: Text)),
		(isJust pingNonce, "nonce" .= maybe "" id pingNonce)]
	toJSONObject (Pong pongNonce) = jsonObject $ listIf [
		(True, "type" .= ("PONG" :: Text)),
		(isJust pongNonce, "nonce" .= maybe "" id pongNonce)]
	toJSONObject Reconnect = jsonObject [
		"type" .= ("RECONNECT" :: Text)]

instance ToJSON SymmetricalPubSubMessage where
	toJSON = Aeson.Object . toJSONObject

instance ToBytes SymmetricalPubSubMessage where
	toBytes = Aeson.encode . toJSON

instance ToJSONObject MessageToPubSub where
	toJSONObject Listen{..} = jsonObject [
		"type" .= ("LISTEN" :: Text),
		"data" .= jsonObject [
			"topics" .= lpsmEventTypes,
			"auth_token" .= lpsmAuthToken ]]

instance ToJSON MessageToPubSub where
	toJSON = Aeson.Object . toJSONObject

instance ToBytes MessageToPubSub where
	toBytes = Aeson.encode . toJSON

instance Messagable RewardRedemption where
	msg RewardRedemption{..} = rrUser ++ "'s " ++ msg rrTitle ++
		(maybe "" ((" writing "++) . msg) rrUserInput) ++
		" <" ++ rrRewardID ++ "/" ++ rrID ++ ">"
