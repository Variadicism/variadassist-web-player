module Variadassist.WebPlayer.Web.PubSub (
	pingPongIntervalSeconds,
	pubSubTopics,
	initPubSub
) where

import Utils.Bytes
import Utils.Fallible
import Utils.Fallible.Trans
import Utils.HATs
import Utils.JSON (extractValue, objectsOnly, stringsOnly, valueIsOnly)
import Utils.Messaging hiding (err, message, warn)
import Utils.WebSockets

import Variadassist.WebPlayer.Chat (say)
import Variadassist.WebPlayer.Memes (newRewardMeme, previewMeme)
import Variadassist.WebPlayer.OverlayAlerts (enqueueOverlayAlertByName)
import Variadassist.WebPlayer.Paths (soundEffectsDir)
import Variadassist.WebPlayer.Secrets (streamerUserID)
import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.TextToSpeech (previewTextToSpeech)
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Utils (playSoundToHeadphones)
import Variadassist.WebPlayer.Web.ControlPanel (elseWarn, message, warn)
import Variadassist.WebPlayer.Web.Helix (rejectRedemption)
import Variadassist.WebPlayer.Web.Timers (newOrRestartTimer, newTimerPrototype)
import Variadassist.WebPlayer.Web.Types.ControlPanel (MessageToControlPanel(..))
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(ChannelPointsRedemptionWebMessage))
import Variadassist.WebPlayer.Web.Types.PubSub
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

import Control.Monad ((>=>))
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.KeyMap as Aeson
import Data.List (find, isPrefixOf)
import Data.List.Extra (dropPrefix)
import Data.Maybe (isJust)
import Data.IORef (atomicModifyIORef, newIORef)
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text
import System.FilePath ((</>))

-- constants

pingPongIntervalSeconds = 10

pubSubTopics = [
	"channel-points-channel-v1." ]

timedRewards = [
	("Emote-Only Chat", 120),
	("Sing Mode", 300),
	("Use Default", 600),
	("BEEEEANS!", 600),
	("Charm Me", 600) ]

priorityRewardPrefixes = [
	"Use Default",
	"Charm Me",
	"BEEEEANS",
	"Sing Mode",
	"Emote-Only Chat",
	"Glasses On",
	"Glasses Off",
	"Narrate",
	"5 Squats",
	"5 Push-Ups",
	"Woc",
	"Name ",
	"Poll",
	"Half Health",
	"Rain Song",
	"One Man's Trash",
	"Aggro All",
	"Sign",
	"Sunshake",
	"Revival Baguette",
	"Ban ",
	"Set Time",
	"Add a Command" ]

rewardRedemptionSoundPath = soundEffectsDir </> "reward glockenspiel.mp3"

rewardRedemptionVolume = 80

-- private utils

pubSubMessagesOnly :: MessageTransformer Aeson.Object
pubSubMessagesOnly = jsonMessageOnly >=> objectsOnly >=>
	valueIsOnly "action" ("from PubSub" :: Text) >=>
	extractValue "content" >=>
	objectsOnly

readRewardRedemption :: Aeson.Object -> Fallible RewardRedemption
readRewardRedemption rewardBody = do
	redemptionInfo <- (
		extractValue "data" >=>
		objectsOnly >=>
		extractValue "redemption" >=>
		objectsOnly) rewardBody
	redemptionID <- (extractValue "id" >=> stringsOnly) redemptionInfo
	let userInputOrErr = (extractValue "user_input" >=> stringsOnly) redemptionInfo
	user <- (
		extractValue "user" >=>
		objectsOnly >=>
		extractValue "display_name" >=>
		stringsOnly) redemptionInfo

	rewardInfo <- (extractValue "reward" >=> objectsOnly) redemptionInfo
	rewardID <- (extractValue "id" >=> stringsOnly) rewardInfo
	title <- (extractValue "title" >=> stringsOnly) rewardInfo

	let priority = any (`isPrefixOf` (Text.unpack title)) priorityRewardPrefixes

	return RewardRedemption {
		rrID = Text.unpack redemptionID,
		rrRewardID = Text.unpack rewardID,
		rrTitle = Text.unpack title,
		rrUser = Text.unpack user,
		rrUserInput = fmap Text.unpack $ toMaybe userInputOrErr,
		rrPriority = priority }

handleRewardRedemption :: RewardRedemption -> VoidFallible
handleRewardRedemption rr@RewardRedemption{..} = do
	sendJSONToPanels $ ChannelPointsRedemptionWebMessage rr
	if rrTitle == "Choose Meme" then do
		playSound
		debugM $ "Choose Meme redeemed with " ++ msg rrUserInput ++ "; building meme..."
		(holdFal $ newRewardMeme rr) >>= \case
			Error error -> do
				rejectRedemption rr
				say $ "Sorry, @" ++ rrUser ++ ", but that URL work: " ++ error
				warn $ "This meme was refunded because of an error: " ++ error
			Falue meme -> previewMeme meme
	else if "Overlay Alert: " `isPrefixOf` rrTitle then do
		let alertName = dropPrefix "Overlay Alert: " rrTitle
		message $ "Overlay Alert: " ++ msg alertName ++ " reward redeemed! Enqueueing..."
		enqueueOverlayAlertByName (alertName) $ Just rr
	else if rrTitle == "Text to Speech" then
		case rrUserInput of
			Nothing -> do
				warn "Text to Speech reward redeemed without input! Refunding..."
				rejectRedemption rr
			Just userInput -> do
				message "Text to Speech reward redeemed! Enqueueing..."
				previewTextToSpeech userInput rr
	-- TODO: figure out how to improve this stupid isJust-(Just) pattern.
	else if isJust $ maybeRewardTime timedRewards then do
		playSound
		let (Just time) = maybeRewardTime timedRewards
		message $ rrTitle ++ " reward redeemed! Making timer..."
		newOrRestartTimer rrTitle time
		success
	-- TODO: use newTimerPrototype like the timedRewards above,
	-- but figure out how to append relevant info to the ID.
	else if "Ban an In-Game Action" == rrTitle then do
		playSound
		message "Ban an In-Game Action redeemed! Making timer..."
		flip newTimerPrototype 600 $ "Ban an In-Game Action: " ++ maybe "???" id rrUserInput
	else if rrTitle == "One Man's Trash" then do
		playSound
		message "One Man's Trash reward redeemed! Making timer..."
		flip newTimerPrototype 600 $ "One Man's Trash from " ++ rrUser
	else do
		playSound
		debugM $ "Unrecognized reward " ++ msg rrTitle ++ "; ignoring..."
	where
		playSound = elseWarn $ playSoundToHeadphones rewardRedemptionSoundPath rewardRedemptionVolume
		maybeRewardTime = fmap snd . find ((==rrTitle) . fst)

-- exported utils

initPubSub :: VoidFallible
initPubSub = do
	State{..} <- tryIO getState

	debugM "Requesting listening to topics from Twitch..."
	sendJSON controlPanelSocket $ ToPubSubWebMessage $ Listen {
		lpsmEventTypes = map (++streamerUserID) pubSubTopics,
		lpsmAuthToken = streamAuthToken }

	nonceRef <- tryIO $ newIORef 0
	let pingPongTransformer :: Text -> (Maybe String -> SymmetricalPubSubMessage) -> MessageTransformer SymmetricalPubSubMessage = \body messageConstructor message -> do
		pingPongBody <- (pubSubMessagesOnly >=> valueIsOnly "type" body) message
		case Aeson.lookup "nonce" pingPongBody of
			Nothing -> return $ messageConstructor Nothing
			Just nonce -> fmap (messageConstructor . Just . Text.unpack) $
				stringsOnly nonce

	debugM "Starting PubSub ping pong..."
	flip pingPongWith controlPanelSocket defaultPingPongOptions{
		ppoPingTransformer = pingPongTransformer "PING" Ping,
		ppoPongTransformer = pingPongTransformer "PONG" Pong,
		ppoSendPing = \socket -> sendJSON socket . SymPubSubWebMessage,
		ppoSendPong = \socket -> sendJSON socket . SymPubSubWebMessage,
		ppoMakePingExpectingPong = do
			nonce <- atomicModifyIORef nonceRef $ \nonce -> (nonce + 1, nonce)
			return (Ping $ Just $ show nonce, const $ return ()),
		ppoPongForPing = \(Ping nonceReceived) ->
			return $ Pong $ fmap show nonceReceived,
		ppoOnNoPong = sendJSON controlPanelSocket $ SymPubSubWebMessage Reconnect,
		ppoSeconds = pingPongIntervalSeconds }

	-- TODO: when PubSub sends Reconnect, reconnect.

	debugM "Beginning watch for events from PubSub..."
	whenMessageAs "obey Channel Points rewards"
		(pubSubMessagesOnly >=>
			valueIsOnly "type" ("MESSAGE" :: Text) >=>
			extractValue "data" >=>
			objectsOnly >=>
			extractValue "message" >=>
			stringsOnly >=>
			fromEither . Aeson.eitherDecode . toBytes >=>
			valueIsOnly "type" ("reward-redeemed" :: Text)) controlPanelSocket $
		\rewardBody -> do
			redemption <- falT $ readRewardRedemption rewardBody
			haltableAsync ("handle " ++ show redemption) $ handleRewardRedemption redemption
			return True