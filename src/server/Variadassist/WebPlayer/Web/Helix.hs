module Variadassist.WebPlayer.Web.Helix (
	fetchHelix,
	fetchHelixAs,
	fetchHelixAsChatBot,
	postHelix,
	postHelixAs,
	postHelixAsChatBot,
	fulfillRedemption,
	rejectRedemption,
	makeChannelPointsReward,
	makeStreamMarker,
	deleteChatMessage
) where

import Utils.Fallible.Trans
import Utils.JSON (encode, jsonObject)
import Utils.HTTP ((/:), HTTPQueryParams, URL, https, fetchJSON, postJSON)
import Utils.Lists (ifNotNull)
import Utils.Maps (intercalateMap)
import Utils.Messaging hiding (err, message, warn)
import Utils.Processes

import Variadassist.WebPlayer.Secrets (botClientID, botUserID, streamerClientID, streamerUserID)
import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption(..))

import Data.Aeson ((.=), ToJSON)
import qualified Data.Aeson as Aeson
import Data.Map (Map)
import qualified Data.Map as Map
import System.Process (proc)

-- constants

twitchHelixURL = "https://api.twitch.tv/helix"

twitchHelixURLReq :: URL
twitchHelixURLReq = https "api.twitch.tv" /: "helix"

-- private utils

callHelix :: ToJSON j =>
	String -> Bool -> String -> String -> Map String String -> Maybe j -> VoidFallible
callHelix actionName needsStreamerAuth method path queryParams maybeBody = do
	State{..} <- tryIO getState
	let (clientID, authToken) = if needsStreamerAuth
		then (streamerClientID, streamAuthToken)
		else (botClientID, chatAuthToken)

	(stdout, stderr) <- exec actionName $ proc "curl" $ [
		"-X", method,
		twitchHelixURL ++ path ++ (ifNotNull ("?"++) $ intercalateMap "=" "&" queryParams),
		"-H", "client-id: " ++ clientID,
		"-H", "Authorization: Bearer " ++ authToken,
		"-H", "Content-Type: application/json"] ++
		maybe [] (\body -> ["-d", encode body ]) maybeBody
	-- TODO: make some cURL tools to read the response and check for good status
	debugM $ "Stdout: " ++ stdout
	debugM $ "Stderr: " ++ stderr


updateRedemptionStatus :: String -> RewardRedemption -> VoidFallible
updateRedemptionStatus newStatus RewardRedemption{..} =
	callHelix ("reward redemption status -> " ++ newStatus) True
		"PATCH" "/channel_points/custom_rewards/redemptions"
		(Map.fromList [("broadcaster_id", streamerUserID), ("reward_id", rrRewardID), ("id", rrID)]) $
		Just $ jsonObject ["status" .= newStatus]

withStreamAuth :: (String -> String -> IOFallible a) -> IOFallible a
withStreamAuth doAsStreamer =
	tryIO getState >>= \State{..} -> doAsStreamer streamAuthToken streamerClientID

withChatAuth :: (String -> String -> IOFallible a) -> IOFallible a
withChatAuth doAsChatBot =
	tryIO getState >>= \State{..} -> doAsChatBot chatAuthToken botClientID

-- exported utils

fetchHelixAs :: String -> String -> String -> [String] -> HTTPQueryParams -> IOFallible Aeson.Value
fetchHelixAs authToken clientID reason pathPieces queryParams =
	fetchJSON reason (foldl (/:) twitchHelixURLReq pathPieces)
		queryParams
		[("Authorization", "Bearer " ++ authToken), ("client-id", clientID)]

fetchHelix :: String -> [String] -> HTTPQueryParams -> IOFallible Aeson.Value
fetchHelix reason pathPieces queryParams = withStreamAuth \authToken clientID ->
	fetchHelixAs authToken clientID reason pathPieces queryParams

fetchHelixAsChatBot :: String -> [String] -> HTTPQueryParams -> IOFallible Aeson.Value
fetchHelixAsChatBot reason pathPieces queryParams = withChatAuth \authToken clientID ->
	fetchHelixAs authToken clientID reason pathPieces queryParams

postHelixAs :: ToJSON j =>
	String -> String -> String -> [String] -> HTTPQueryParams -> Maybe j -> IOFallible Aeson.Value
postHelixAs authToken clientID reason pathPieces queryParams maybeBody =
	postJSON reason (foldl (/:) twitchHelixURLReq pathPieces)
		queryParams
		[("Authorization", "Bearer " ++ authToken), ("client-id", clientID)]
		maybeBody

postHelix :: ToJSON j => (String -> [String] -> HTTPQueryParams -> Maybe j -> IOFallible Aeson.Value)
postHelix reason pathPieces queryParams maybeBody =
	withStreamAuth \authToken clientID ->
		postHelixAs authToken clientID reason pathPieces queryParams maybeBody

postHelixAsChatBot :: ToJSON j =>
	(String -> [String] -> HTTPQueryParams -> Maybe j -> IOFallible Aeson.Value)
postHelixAsChatBot reason pathPieces queryParams maybeBody =
	withChatAuth \authToken clientID ->
		postHelixAs authToken clientID reason pathPieces queryParams maybeBody

fulfillRedemption :: (RewardRedemption -> VoidFallible)
fulfillRedemption = updateRedemptionStatus "FULFILLED"

rejectRedemption :: (RewardRedemption -> VoidFallible)
rejectRedemption = updateRedemptionStatus "CANCELED"

makeChannelPointsReward :: String -> Int -> VoidFallible
makeChannelPointsReward name cost =
	callHelix "make reward" True
		"POST" "/channel_points/custom_rewards"
		(Map.singleton "broadcaster_id" streamerUserID) $
		Just $ jsonObject ["title" .= name, "cost" .= cost]

makeStreamMarker :: String -> IOFallible Aeson.Value
makeStreamMarker description =
	postHelix ("mark stream " ++ show description)
		["streams", "markers"] [] $
		Just $ jsonObject ["user_id" .= streamerUserID, "description" .= description]

deleteChatMessage :: String -> VoidFallible
deleteChatMessage messageID =
	callHelix "delete chat message" False
		"DELETE" "/moderation/chat"
		(Map.fromList [("broadcaster_id", streamerUserID),
			("moderator_id", botUserID), ("message_id", messageID)])
		(Nothing :: Maybe Aeson.Value)