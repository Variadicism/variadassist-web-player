module Variadassist.WebPlayer.Web.EventSub (
	followSoundPath,
	eventSubMessagesOnly,
	eventSubEventsOnly,
	initEventSub
) where

import Utils.Fallible.Trans
import Utils.HATs (haltableAsync)
import Utils.JSON (extractValue, jsonObject, objectsOnly, stringsOnly, valueIsOnly)
import Utils.Messaging (debugM, msg)
import Utils.WebSockets (MessageTransformer, jsonMessageOnly, whenMessageAs)

import Variadassist.WebPlayer.Paths (soundEffectsDir)
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Secrets (streamerUserID)
import Variadassist.WebPlayer.State (getState)
import Variadassist.WebPlayer.Utils (playSound)
import Variadassist.WebPlayer.Web.Helix (postHelix)
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(FollowWebMessage, SubscribeWebMessage))
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

import Control.Monad ((>=>))
import Data.Aeson ((.=))
import qualified Data.Aeson as Aeson
import Data.Function ((&))
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text
import System.FilePath ((</>))

-- constants

followSoundPath = soundEffectsDir </> "follow.mp3"

followSoundVolume = 100

subscribeSoundPath = soundEffectsDir </> "follow.mp3"

subscribeSoundVolume = 100

-- private

subToEventsVersion :: Int -> String -> String -> VoidFallible
subToEventsVersion version eventType sessionID = do
	State{..} <- tryIO getState
	postHelix ("subscribe to " ++ eventType ++ " events") ["eventsub", "subscriptions"] [] $
		Just $ jsonObject [
			"type" .= (Text.pack $ "channel." ++ eventType),
			"version" .= (Text.pack $ show version),
			"condition" .= jsonObject [
				"broadcaster_user_id" .= streamerUserID,
				"moderator_user_id" .= streamerUserID ],
			"transport" .= jsonObject [
				"method" .= ("websocket" :: Text),
				"session_id" .= sessionID ] ]
	success

subToEvents = subToEventsVersion 1

initSession :: String -> VoidFallible
initSession sessionID = do
	State{..} <- tryIO getState
	subToEventsVersion 2 "follow" sessionID
	subToEvents "subscribe" sessionID
	whenMessageAs "listen for EventSub events"
		eventSubEventsOnly controlPanelSocket
		\(eventType, eventData) -> do
			case eventType of
				"channel.follow" -> do
					playSound followSoundPath followSoundVolume
					username <- falT $ eventData &
						extractValue "user_name" >>= stringsOnly
					sendJSONToPanels $ FollowWebMessage $ Text.unpack username
				"channel.subscribe" -> do
					playSound subscribeSoundPath subscribeSoundVolume
					username <- falT $ eventData &
						extractValue "user_name" >>= stringsOnly
					sendJSONToPanels $ SubscribeWebMessage $ Text.unpack username
				_ -> debugM $ msg eventType ++ " isn't a recognized EventSub" ++
					" notification type; ignoring..."
			return True

-- exported

eventSubMessagesOnly :: MessageTransformer Aeson.Object
eventSubMessagesOnly = jsonMessageOnly >=> objectsOnly >=>
	valueIsOnly "action" ("from EventSub" :: Text) >=>
	extractValue "content" >=>
	objectsOnly

eventSubTypeOnly :: String -> MessageTransformer Aeson.Object
eventSubTypeOnly expectedType = eventSubMessagesOnly >=> \eventSubMessage -> do
	eventSubMessage &
		extractValue "metadata" >>= objectsOnly >>=
		valueIsOnly "message_type" expectedType
	eventSubMessage &
		extractValue "payload" >>= objectsOnly

eventSubEventsOnly :: MessageTransformer (String, Aeson.Object)
eventSubEventsOnly message = do
	payload <- eventSubTypeOnly "notification" message
	eventType <- payload &
		extractValue "subscription" >>= objectsOnly >>=
		extractValue "type" >>= stringsOnly
	eventData <- payload & extractValue "event" >>= objectsOnly
	return (Text.unpack eventType, eventData)

initEventSub :: VoidFallible
initEventSub = do
	State{..} <- tryIO getState
	whenMessageAs "listen for EventSub session ID"
		(eventSubTypeOnly "session_welcome")
		controlPanelSocket
		\payload -> do
			sessionID <- falT $ payload &
				extractValue "session" >>= objectsOnly >>=
				extractValue "id" >>= stringsOnly
			haltableAsync "run EventSub session" $ initSession $ Text.unpack sessionID
			return False