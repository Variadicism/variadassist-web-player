module Variadassist.WebPlayer.Memes (
	videoSizeLimit,
	memesDir,
	newStreamerMeme,
	newRewardMeme,
	initMemeDisplayQueue,
	displayMeme,
	displayMemeURL,
	displayCopiedMeme,
	hideMeme,
	previewMeme,
	acceptNextMeme,
	discardNextMeme,
	discardNextMemeWithoutRefund
) where

import Utils.Fallible.Trans
import Utils.Messaging hiding (err, message, warn)
import Utils.Processes hiding (message)
import Utils.Queues
import Utils.Regex ((=~))
import Utils.Regexes (urlRegex)
import Utils.WebSockets
import Variadassist.Twitch.Secrets (streamerUsername)

import Variadassist.WebPlayer.Chat (say)
import Variadassist.WebPlayer.Paths (tempDir)
import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Types.Memes
import Variadassist.WebPlayer.Web.ControlPanel (err, message, warn)
import Variadassist.WebPlayer.Web.Helix (fulfillRedemption, rejectRedemption)
import Variadassist.WebPlayer.Web.Ports (memesDirPort)
import Variadassist.WebPlayer.Web.Types.ControlPanel (MessageToControlPanel(..))
import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption(..))
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(..))
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)
import Variadassist.WebPlayer.Types.State (State(..))

import Control.Concurrent (threadDelay)
import Data.Char (isNumber, toLower)
import Data.List (find, isPrefixOf)
import Data.List.Extra (dropPrefix, takeWhileEnd)
import Data.String.Utils (strip)
import Text.Read (readMaybe)
import System.Clipboard (getClipboardString)
import System.Directory (listDirectory)
import System.FilePath ((</>))
import System.Process (proc)

-- constants

videoSizeLimit = 50000000  -- 50MB

memesDir :: FilePath
memesDir = tempDir </> "memes"

imageFileExtensions = [
	"ase",
	"art",
	"bmp",
	"blp",
	"cd5",
	"cit",
	"cpt",
	"cr2",
	"cut",
	"dds",
	"dib",
	"djvu",
	"egt",
	"exif",
	"gif",
	"gpl",
	"grf",
	"icns",
	"ico",
	"iff",
	"jng",
	"jpeg",
	"jpg",
	"jfif",
	"jp2",
	"jps",
	"lbm",
	"max",
	"miff",
	"mng",
	"msp",
	"nef",
	"nitf",
	"ota",
	"pbm",
	"pc1",
	"pc2",
	"pc3",
	"pcf",
	"pcx",
	"pdn",
	"pgm",
	"pi1",
	"pi2",
	"pi3",
	"pict",
	"pct",
	"pnm",
	"pns",
	"ppm",
	"psb",
	"psd",
	"pdd",
	"psp",
	"px",
	"pxm",
	"pxr",
	"qfx",
	"raw",
	"rle",
	"sct",
	"sgi",
	"rgb",
	"int",
	"bw",
	"tga",
	"tiff",
	"tif",
	"vtf",
	"xbm",
	"xcf",
	"xpm",
	"3dv",
	"amf",
	"ai",
	"awg",
	"cgm",
	"cdr",
	"cmx",
	"dxf",
	"e2d",
	"egt",
	"eps",
	"fs",
	"gbr",
	"odg",
	"svg",
	"stl",
	"vrml",
	"x3d",
	"sxd",
	"v2d",
	"vnd",
	"wmf",
	"emf",
	"art",
	"xar",
	"png",
	"webp",
	"jxr",
	"hdp",
	"wdp",
	"cur",
	"ecw",
	"iff",
	"lbm",
	"liff",
	"nrrd",
	"pam",
	"pcx",
	"pgf",
	"sgi",
	"rgb",
	"rgba",
	"bw",
	"int",
	"inta",
	"sid",
	"ras",
	"sun",
	"tga",
	"heic",
	"heif" ]

-- private utils

fileExtensionFromURL :: String -> Maybe String
fileExtensionFromURL url = do
	let fileExtensionFromURL = takeWhileEnd (not . (=='.')) url
	if length fileExtensionFromURL > 4
		then Nothing
		else Just fileExtensionFromURL

convertVideoMeme :: String -> String -> IOFallible String
convertVideoMeme url altExtension = do
	memeFileNames <- tryIO $ listDirectory memesDir
	let fileExtension = maybe altExtension id $ fileExtensionFromURL url
	let fileNameNoExt = "meme " ++ show (length memeFileNames)
	let videoFileName = fileNameNoExt ++ "." ++ fileExtension
	let gifFileName = fileNameNoExt ++ ".gif"
	exec ("download " ++ msg url) $
		proc "curl" [url, "--output", memesDir </> (fileNameNoExt ++ "." ++ fileExtension)]
	exec ("convert " ++ msg url ++ " to GIF") $
		proc "ffmpeg" [
			"-i", memesDir </> videoFileName,
			"-filter_complex", "[0:v] fps=12,scale=-1:540,split [a][b];[a] palettegen [p];[b][p] paletteuse",
			memesDir </> gifFileName ]
	return $ "http://localhost:" ++ show memesDirPort ++ "/" ++ gifFileName

convertOrPassMeme :: String -> IOFallible String
convertOrPassMeme url = do
	debugM $ "Checking meme type at " ++ msg url ++ "..."
	if "data:" `isPrefixOf` url then do
		debugM $ "Data image URL found; no conversion needed!"
		return url
	else do
		(headerInfo, _) <- exec ("get content type of " ++ msg url) $
			proc "curl" ["--head", url]
		let headers = lines $ map toLower headerInfo
		case find ("content-type:" `isPrefixOf`) headers of
			Nothing -> do
				debugM $ "No content type was found via cURL for " ++ msg url ++ "; guessing..."
				let extension = maybe "" id $ fileExtensionFromURL url
				if extension `elem` imageFileExtensions then do
					debugM $ "Meme is image (ext. " ++ msg extension ++
						"); no conversion needed!"
					return url
				else do
					debugM $ "Meme is not an image; attempting to convert..."
					convertVideoMeme url "vid"
			Just contentTypeLine -> do
				let memeType = strip $ dropPrefix "content-type:" contentTypeLine
				if "image/" `isPrefixOf` memeType then do
					debugM $ "Meme is image (" ++ msg memeType ++ "); no conversion needed!"
					return url
				else if "video/" `isPrefixOf` memeType then do
					debugM $ "Meme is video (" ++ msg memeType ++
						"); downloading and converting..."
					let contentLength = maybe 0 id do
						lengthHeader <- find ("content-length:" `isPrefixOf`) headers
						let lengthString = takeWhileEnd isNumber lengthHeader
						readMaybe lengthString :: Maybe Integer
					if contentLength > videoSizeLimit
						then errorT "The meme video is too big!"
						else convertVideoMeme url $ takeWhileEnd (not . (=='/')) memeType
				else if "text/html" == memeType then
					errorT "This appears to be a web page! Please get a direct link to an image or video file. On a desktop or laptop, try right-clicking and using 'open in new tab'; on mobile, try a long press for more options."
				else
					errorT $ "The meme type " ++ msg memeType ++ " was not recognized!"

dequeueMemePreview :: IOFallible (Maybe Meme)
dequeueMemePreview = do
	(maybePreviewedMeme, controlPanelSocket) <- tryIO $ statefullyOutputting $ \state@State{..} ->
		let (maybePreviewedMeme, newMemePreviewQueue) = dequeue memePreviewQueue
		in (state{ memePreviewQueue = newMemePreviewQueue }, (maybePreviewedMeme, controlPanelSocket))

	case maybePreviewedMeme of
		Nothing -> warn "There are no memes in the judgement queue!"
		Just previewedMeme -> do
			debugM $ "Dequeueing next meme at " ++ msg previewedMeme ++ "..."
			sendJSON controlPanelSocket DequeueMemePreviewMessage

	return maybePreviewedMeme

newMeme :: String -> String -> Maybe RewardRedemption -> IOFallible Meme
newMeme url user rr = do
	displayURL <- convertOrPassMeme url
	return Meme {
		memeURL = url,
		memeDisplayURL = displayURL,
		memeUser = user,
		memeRR = rr }

-- exported utils

newStreamerMeme :: String -> IOFallible Meme
newStreamerMeme url = newMeme url streamerUsername Nothing

newRewardMeme :: RewardRedemption -> IOFallible Meme
newRewardMeme rr@RewardRedemption{..} = case rrUserInput of
	Nothing -> errorT $ redemptionErrorPrefix ++ "The user didn't provide a meme!"
	Just userInput -> case userInput =~ urlRegex of
		Nothing -> errorT $ redemptionErrorPrefix ++ "The user didn't provide a recognizable URL!"
		Just (url, _, _) -> prependToErrorT redemptionErrorPrefix $ newMeme url rrUser $ Just rr
	where
		redemptionErrorPrefix = "The " ++ rrTitle ++ " redemption was rejected due to an error: "

initMemeDisplayQueue :: IO (TimedQueue Meme)
initMemeDisplayQueue = newTimedQueue "display memes" 1 $ \meme@Meme{..} -> do
	let optionalVideoWarning = if memeURL == memeDisplayURL then "" else
		" FYI, the meme linked might have audio not checked by " ++ streamerUsername ++
		"; follow it for any original audio at your own risk!"
	message $ "Displaying meme " ++ msg meme ++ "..."
	say $ "@" ++ memeUser ++ ", " ++ memeURL ++ " is up on stream!" ++ optionalVideoWarning
	sendJSONToPanels $ ShowMemeWebMessage memeDisplayURL memeUser
	tryIO $ threadDelay $ 60 * 1000000
	sendJSONToPanels HideMemeWebMessage

displayMeme :: Meme -> VoidFallible
displayMeme meme = do
	message $ "Enqueueing meme " ++ msg meme ++ "..."
	State{..} <- tryIO getState
	enqueueTimed memeDisplayQueue meme

displayMemeURL :: String -> VoidFallible
displayMemeURL url = newStreamerMeme url >>= displayMeme

displayCopiedMeme :: VoidFallible
displayCopiedMeme = do
	debugM "Showing image from URL on clipboard..."
	maybeClipboard <- tryIO getClipboardString
	case maybeClipboard of
		Nothing -> warn "You haven't copied anything!"
		Just clipboard -> case clipboard =~ urlRegex of
			Nothing -> warn $ "Your clipboard, " ++ msg clipboard ++ ", is not a URL!"
			Just (url, _, _) -> newStreamerMeme url >>= displayMeme

hideMeme :: VoidFallible
hideMeme = sendJSONToPanels HideMemeWebMessage

previewMeme :: Meme -> VoidFallible
previewMeme meme@Meme{..} = do
	debugM $ "Previewing " ++ msg meme ++ "..."
	State{..} <- tryIO getState
	sendJSON controlPanelSocket $ PreviewMemeWebMessage memeDisplayURL memeUser
	tryIO $ statefully $ \state@State{..} -> state{ memePreviewQueue = enqueue memePreviewQueue meme }

acceptNextMeme :: VoidFallible
acceptNextMeme = do
	message "Displaying next meme..."
	maybeNextMeme <- dequeueMemePreview
	case maybeNextMeme of
		Nothing -> success
		Just nextMeme -> do
			case memeRR nextMeme of
				Nothing -> debugM "No reward redemption to mark as fulfilled!"
				Just rr -> fulfillRedemption rr
			displayMeme nextMeme

discardNextMemeWithoutRefund :: VoidFallible
discardNextMemeWithoutRefund = do
	message "Discarding next meme..."
	dequeueMemePreview
	success

discardNextMeme :: VoidFallible
discardNextMeme = do
	message "Discarding and refunding next meme..."
	maybeMeme <- dequeueMemePreview
	case maybeMeme of
		Nothing -> warn "There are no memes in the queue to be refunded!"
		Just meme -> case memeRR meme of
			Nothing -> err $ "The meme to be refunded, " ++ msg meme ++ ", had no redemption attached to refund!"
			Just rr -> rejectRedemption rr
