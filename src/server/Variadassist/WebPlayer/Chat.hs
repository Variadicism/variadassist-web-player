module Variadassist.WebPlayer.Chat (
	initChat,
	leaveChat,
	say,
) where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.JSON (extractValue, objectsOnly, stringsOnly, valueIsOnly)
import Utils.Lists (wrap)
import Utils.Messaging (debugM, msg)
import Utils.Regex ((=~), (==~), (*//~), rei, resi)
import Utils.Regexes (urlRegex)
import Utils.WebSockets (MessageTransformer, jsonMessageOnly, sendJSON, whenMessageAs)

import Variadassist.WebPlayer.State
import Variadassist.WebPlayer.Types.Chat
import Variadassist.WebPlayer.Types.Memes (chooseMemeRewardID)
import Variadassist.WebPlayer.Paths (soundEffectsDir)
import Variadassist.WebPlayer.Types.State
import Variadassist.WebPlayer.Utils (playSoundToHeadphones)
import Variadassist.WebPlayer.Web.ControlPanel (elseWarn, message, warn)
import Variadassist.WebPlayer.Web.Helix (deleteChatMessage)
import Variadassist.WebPlayer.Web.Types.ControlPanel (MessageToControlPanel(FromChatWebMessage, ToIRCWebMessage))

import Control.Monad ((>=>), when)
import Data.List (intercalate)
import Data.List.Extra (splitOn)
import Data.Map ((!), (!?))
import qualified Data.Map as Map
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text
import System.FilePath ((</>))
import System.Random (getStdRandom, uniformR)

-- constants

twitchChatBotName = "Variadassist"

greetingsToUse = [
	"Hi", "Hello", "Heyo", "Hi there", "Hey there", "Hiya", "Greetings",
	"Konnichiwa", "Bienvenidos", "Hallo"]

greetingRegex = [rei|\b(
	# matches with chat bot's own greetings
	hi|hello|heyo|hey|hiya|greeting|
	konn?ichiwah?|bi?enveni?dos|hallo|
	# greetings others may use
	howdy|yo|wh?a[sz][sz]?up|sup|t?here|
	how'?(s|re?)?|
	(what|wu[sz])'?[sz]?.*(g(oo|u)d|up|in['g]?)\b|
	it'?s.*been|
	long.*no.*(see|hear|here?)|
	(good|nice|plea?sure?).*to.*(see|hear|meet)|
	morn|afte?rn|ev[e']?nin|g.*day
)|]

setIntroRegex = [rei|\b(intro(duce?)?)(\sme)?(\s(as|with))?|]

chatSoundPath = soundEffectsDir </> "chat whoosh.mp3"

chatVolume = 70

-- private utils

ircMessagesOnly :: MessageTransformer Text
ircMessagesOnly = jsonMessageOnly >=> objectsOnly >=>
	valueIsOnly "action" ("from IRC" :: Text) >=> extractValue "content" >=>
	stringsOnly

parseIRCMessageBlock :: (String -> Fallible [IRCMessage])
parseIRCMessageBlock = sequence . map parseIRCString . filter (not . null) . splitOn "\r\n"

send :: [String] -> [String] -> VoidFallible
send command args = do
	State{..} <- tryIO getState
	sendJSON controlPanelSocket $ ToIRCWebMessage $ simpleIRCMessage command args

onMessageToVariadassist :: String -> String -> VoidFallible
onMessageToVariadassist message{-without tags-} username
	| message ==~ greetingRegex = do
		i <- getStdRandom $ uniformR (0, length greetingsToUse - 1)
		say $ (greetingsToUse !! i) ++ ", @" ++ username ++ "!"
	| message ==~ setIntroRegex = case message =~ urlRegex of
		Nothing -> say $ "@" ++ username ++ ", I don't recognize your intro's URL!"
		Just (url, _, _) -> do
			tryIO $ statefully \s -> s{ viewerData = (viewerData s){
				intros = Map.insert username url s.viewerData.intros } }
			say $ "Got it, @" ++ username ++ "! " ++
				"I'll show that when you enter the stream as long as you're a subscriber! " ++
				"[Note: this feature is coming soon!]"
	| otherwise = warn $ "Variadassist was tagged in " ++ msg message ++
		", but I couldn't understand the message!"

handleChatMessage :: String -> IRCMessage -> VoidFallible
handleChatMessage messageContent IRCMessage{..} = do
	elseWarn $ playSoundToHeadphones chatSoundPath chatVolume
	case ircmTags !? "custom-reward-id" of
		Nothing -> do
			State{..} <- tryIO getState
			sendJSON controlPanelSocket $ FromChatWebMessage ircmOrigin messageContent
			when (messageContent ==~ [rei|@Variadassist|]) $ case ircmOrigin of
			-- TODO: consider https://hackage.haskell.org/package/fuzzy-0.1.0.1/docs/Text-Fuzzy.html
				Just (TwitchUser username) -> do
					let messageWithoutTags = messageContent *//~ [resi|@Variadasist///|]
					onMessageToVariadassist messageWithoutTags username
				_ -> warn $ "The chat bot was tagged in " ++ msg messageContent ++
					", but the sender is " ++ msg ircmOrigin ++ ", not a Twitch user! Spooky! :O"
		Just rewardID -> if rewardID /= chooseMemeRewardID then success else
			deleteChatMessage $ ircmTags ! "id"

handleIRCMessage :: IRCMessage -> VoidFallible
handleIRCMessage ircMessage@IRCMessage{..} = do
	debugM $ "Handling " ++ show ircMessage ++ "..."
	case (ircmCommand, ircmArgs) of
		(["PING"], _) -> do
			debugM "Received IRC PING; sending PONG..."
			send ["PONG"] ircmArgs
		(["PRIVMSG"], [_, messageContent]) -> handleChatMessage messageContent ircMessage
		(["JOIN"], _) -> message $ show ircmOrigin ++ " is here!!"
		(["PART"], _) -> message $ show ircmOrigin ++ " left. :("
		-- TODO: consider decreasing the visibility of this message later.
		("NOTICE":subcommands, _) -> message $ "Twitch IRC notice" ++
			(if null subcommands then "" else (" (" ++ intercalate " " subcommands ++ ")")) ++
			" regarding " ++ head ircmArgs ++
			": " ++ (intercalate " " $ tail ircmArgs)
		_ -> debugM $ "Unrecognized " ++ msg (intercalate " " ircmCommand) ++ " message; ignoring..."

-- exported utils

say :: String -> VoidFallible
say message = send ["PRIVMSG"] [ircChannel, message]

initChat :: VoidFallible
initChat = do
	State{..} <- tryIO getState

	whenMessageAs "receive IRC messages" ircMessagesOnly controlPanelSocket $ \ircMessagesText -> do
		let ircMessagesString = Text.unpack ircMessagesText
		debugM $ "Received IRC message block:\n" ++ wrap "\"" ircMessagesString
		case parseIRCMessageBlock ircMessagesString of
			-- TODO: consider decreasing the visibility of this message later.
			Error error -> message $ "There was an error while parsing the IRC message block: " ++ error
			Falue ircMessages -> mapM_ handleIRCMessage ircMessages
		return True

	debugM "Authenticating chat bot with Twitch..."
	send ["CAP", "REQ"] ["twitch.tv/membership twitch.tv/tags twitch.tv/commands"]
	send ["PASS"] ["oauth:" ++ chatAuthToken]
	send ["NICK"] [twitchChatBotName]
	send ["JOIN"] [ircChannel]
	say "Hi! MrDestructoid"

leaveChat :: VoidFallible
leaveChat = send ["PART"] [ircChannel]
