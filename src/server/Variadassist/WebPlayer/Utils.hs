module Variadassist.WebPlayer.Utils (
	playSound,
	playSoundToHeadphones,
	playSoundTo,
	showDuration,
	withChoiceFromState,
	withChoiceFromStateAs
) where

import Utils.Choose (withChoiceAs)
import Utils.Fallible.Trans
import Utils.Messaging (msg)
import Utils.Processes
import Utils.Regex ((=~), re)

import Data.List (findIndex, isInfixOf)
import Data.Maybe (mapMaybe)
import Numeric.Extra (intToFloat)
import System.Process (proc)

import Variadassist.WebPlayer.State (getState, statefully)
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Web.ControlPanel (warn)

headphonesSinkName = "MAJOR IV"

findSink :: String -> IOFallible Int
findSink name = do
	let sinkNumberRegex = [re|Sink\s\#(\d+)|]
	(sinkInfo, _) <- exec "list Pulse sinks" $ silently $ proc "pactl" ["list", "sinks"]
	let sinkInfoLines = lines sinkInfo
	let maybeHeadphonesLineIndex =
		findIndex (("Description: " ++ name) `isInfixOf`) sinkInfoLines
	case maybeHeadphonesLineIndex of
		Nothing -> errorT $ "The " ++ msg name ++ " sink couldn't be found to play a sound!"
		Just headphonesLineIndex -> return $ snd $ last $
			filter (\(i, _) -> i < headphonesLineIndex) $
			map (\(sinkNumberLineIndex, (_, _, [(sinkNumberString, _)])) ->
				(sinkNumberLineIndex, read sinkNumberString :: Int)) $
			mapMaybe (\(i, line) -> fmap (i,) $ line =~ sinkNumberRegex) $
			zip [0..] sinkInfoLines

playSoundTo :: String -> FilePath -> Int -> VoidFallible
playSoundTo device soundPath volume = do
	exec ("play " ++ soundPath) $
		proc "pw-play" ["--target", device, "--volume", show $ intToFloat volume / 100, soundPath]
	success

playSoundToHeadphones :: FilePath -> Int -> VoidFallible
playSoundToHeadphones soundPath volume = do
	state <- tryIO getState
	let findAndSetHeadphonesSink = do
		headphonesSink <- findSink headphonesSinkName
		tryIO $ statefully $ \state -> state{ headphonesSink = Just headphonesSink }
		return headphonesSink
	headphonesSink <- maybe findAndSetHeadphonesSink return $ headphonesSink state
	playSoundTo (show headphonesSink) soundPath volume

playSound :: (FilePath -> Int -> VoidFallible)
playSound = playSoundTo "auto"

showDuration :: (Float -> String)
showDuration = showDenoms True
	where
		showDenoms lowestDenom duration = do
			let showDenom denom = (if denom < 10 then "0" else "") ++
				(if lowestDenom then to2Places denom else show $ floor denom)
			if duration < 60
				then showDenom duration
				else do
					let nextDenom :: Float = fromIntegral $ (floor duration) `div` 60
					let thisDenom = duration - nextDenom*60
					showDenoms False nextDenom ++ ":" ++ showDenom thisDenom
		to2Places float = show $ (fromIntegral $ round $ float * 100 :: Float) / 100

withChoiceFromState :: ((State -> [String]) -> String -> (String -> VoidFallible) -> VoidFallible)
withChoiceFromState = withChoiceFromStateAs id

withChoiceFromStateAs :: (a -> String) -> (State -> [a]) -> String -> (a -> VoidFallible) -> VoidFallible
withChoiceFromStateAs toReadableID choicesFromState typeNamePlural toDo = do
	state <- tryIO getState
	let choices = choicesFromState state
	if null choices
		then warn $ "There are no " ++ typeNamePlural ++ " available!"
		else withChoiceAs toReadableID choices toDo