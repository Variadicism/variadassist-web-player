module Variadassist.WebPlayer.State (
	stateIsPresent,
	initState,
	getState,
	statefully,
	statefullyOutputting
) where

import Utils.Fallible.Trans
import qualified Utils.State as State

import Variadassist.WebPlayer.Types.State

stateVar :: State.StateVar State
stateVar = State.declareState

stateIsPresent :: IO Bool
stateIsPresent = State.stateIsPresent stateVar

initState :: (State -> Void)
initState = State.initState stateVar

statefullyOutputting :: ((State -> (State, a)) -> IO a)
statefullyOutputting = State.statefullyOutputting stateVar

statefully :: ((State -> State) -> Void)
statefully = State.statefully stateVar

getState :: IO State
getState = State.getState stateVar
