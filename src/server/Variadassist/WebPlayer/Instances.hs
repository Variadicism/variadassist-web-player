module Variadassist.WebPlayer.Instances where

import Utils.Messaging (Messagable, msg)

import Data.Char (toUpper)
import Data.List.Extra (dropPrefix)
import Evdev.Codes (Key)

instance Messagable Key where
	msg = (\(l:rest) -> toUpper l:rest) . dropPrefix "Left" . dropPrefix "Key" . show
