module Variadassist.WebPlayer.Commands (
	Command(..),
	commandPipePath,
	executeCommand,
	readPipeCommand
) where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.Messaging hiding (err, message, warn)
import Utils.HATs

import Variadassist.WebPlayer.Memes (acceptNextMeme, discardNextMeme, displayCopiedMeme, displayMemeURL)
import Variadassist.WebPlayer.Music (inquireThenPlayMusicCategory, playMusicCategory, stopMusic)
import Variadassist.WebPlayer.OverlayAlerts (enqueueOverlayAlertByName, inquireThenEnqueueOverlayAlert)
import Variadassist.WebPlayer.Paths (tempDir)
import Variadassist.WebPlayer.Randomizer (randomizers)
import qualified Variadassist.WebPlayer.Randomizer as Randomizer
import Variadassist.WebPlayer.Types.Commands (Command(..))
import Variadassist.WebPlayer.Types.Music (MusicCategory(..))
import Variadassist.WebPlayer.Types.Randomizer (Randomizer(..))
import Variadassist.WebPlayer.Web.ControlPanel (err)
import Variadassist.WebPlayer.Web.Helix (makeChannelPointsReward)

import Data.Char (isSpace)
import Data.List (find, isPrefixOf)
import GHC.Utils.Misc (split)
import System.FilePath ((</>))

-- constants

commandPipePath :: FilePath
commandPipePath = tempDir </> "commands"

-- exported utils

executeCommand :: Command -> VoidFallible
executeCommand command = do
	haltableAsync ("execute pipe command " ++ msgShow command) $ do
		fallible <- holdFal $ case command of
			MakeReward name cost -> makeChannelPointsReward name cost
			StopMusic -> stopMusic
			PlayCategory category -> playMusicCategory category
			ShowOverlayAlert name -> enqueueOverlayAlertByName name Nothing
			ShowMeme url -> displayMemeURL url
			ShowMyMeme -> displayCopiedMeme
			JudgeNextMeme accept -> if accept then acceptNextMeme else discardNextMeme
			PlayCategoryInquire -> inquireThenPlayMusicCategory
			OverlayAlertInquire -> inquireThenEnqueueOverlayAlert
			DisplayRandomizer name -> findRandomizerAnd name Randomizer.display
			SpinRandomizer name -> findRandomizerAnd name Randomizer.spin
		case fallible of
			Error error -> err $ "Execution of " ++ msgShow command ++ " threw an error:\n" ++ error
			Falue _ -> success
	success
	where
		findRandomizerAnd name action = maybe (noRandomizerNamedError name) action $
			find (\r -> randomizerName r == name) randomizers
		noRandomizerNamedError name = errorT $ "There is no randomizer named " ++ msg name ++ "!"

-- TODO: replace this with a standard Read implementation later after the macro board handler is refactored to not use it anymore.
readPipeCommand :: String -> Fallible Command
readPipeCommand command = do
	let (commandName, spaceAndParamsString) = break isSpace command
	let paramsString = dropWhile isSpace spaceAndParamsString
	debug ("Read pipe command " ++ msg command ++ "; interpreting...") $
		case commandName of
			"MAKE_REWARD" -> do
				let [name, costString] = split '@' paramsString
				let cost = read costString :: Int
				return $ MakeReward name cost
			"PLAY_MUSIC_CATEGORY" -> do
				let params = split ',' paramsString
				let starting = "START" `elem` params
				case filter (not . flip elem ["", "START"]) params of
					[category, subcategory] ->
						return $ PlayCategory $ MusicCategory{
							musicCategory = category,
							musicSubcategory = Just subcategory,
							musicStarting = starting }
					[category] ->
						return $ PlayCategory $ MusicCategory{
							musicCategory = category,
							musicSubcategory = Nothing,
							musicStarting = starting }
					[] -> return StopMusic
					filteredParams -> Error $ "Play music category paramters " ++ msgList filteredParams ++ " were not properly formatted!"
			"SHOW_MEME" -> return $ ShowMeme paramsString
			"SHOW_MY_MEME" -> return ShowMyMeme
			"OVERLAY_ALERT" -> return $ ShowOverlayAlert paramsString
			"JUDGE_NEXT_MEME" ->
				return $ JudgeNextMeme $ not $ "DISCARD" `isPrefixOf` paramsString
			"DISPLAY_RANDOMIZER" -> return $ DisplayRandomizer paramsString
			"SPIN_RANDOMIZER" -> return $ SpinRandomizer paramsString
			"?OVERLAY_ALERT" -> return OverlayAlertInquire
			"?PLAY_MUSIC_CATEGORY" -> return PlayCategoryInquire
			unknownCommand -> Error $ "Unknown pipe command: " ++ msg unknownCommand
