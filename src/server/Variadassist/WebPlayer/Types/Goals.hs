module Variadassist.WebPlayer.Types.Goals (
	Goal(..),
	goalMaxDisplayLength,
	pencilWriteSoundPath,
	pencilCrossOutSoundPath,
	pencilCheckBoxSoundPath,
	pencilSoundEffectsVolume
) where

import Utils.Fallible.Trans

import Variadassist.WebPlayer.Paths (soundEffectsDir)

import System.FilePath ((</>))

-- constants

goalMaxDisplayLength :: Int
goalMaxDisplayLength = 25

pencilWriteSoundPath :: FilePath
pencilWriteSoundPath = soundEffectsDir </> "pencil write.mp3"

pencilCrossOutSoundPath :: FilePath
pencilCrossOutSoundPath = soundEffectsDir </> "pencil cross out.mp3"

pencilCheckBoxSoundPath :: FilePath
pencilCheckBoxSoundPath = soundEffectsDir </> "pencil check box.mp3"

pencilSoundEffectsVolume :: Int
pencilSoundEffectsVolume = 40

-- types

data Goal = Goal {
	gName :: String,
	gDisplayName :: String,
	gOnMeet :: VoidFallible,
	gOnFail :: VoidFallible,
	gMarked :: Bool }

instance Eq Goal where
	(==) g1 g2 = gName g1 == gName g2

instance Show Goal where
	show Goal{..} = if gName == gDisplayName
		then gName
		else gName ++ "(\"" ++ gDisplayName ++ "\")"
