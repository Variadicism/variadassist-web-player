module Variadassist.WebPlayer.Types.Music (
	MusicCategory(..),
	Playlist,
	TrackInfo(..)
) where

import Data.Map.Ordered (OMap)
import Sound.HTagLib (Title, Artist)

data MusicCategory = MusicCategory{
	musicCategory :: String,
	musicSubcategory :: Maybe String,
	musicStarting :: Bool
} deriving (Read, Show)

type Playlist = OMap FilePath TrackInfo

data TrackInfo = TrackInfo { trackTitle :: Title, trackArtist :: Artist } deriving Show
