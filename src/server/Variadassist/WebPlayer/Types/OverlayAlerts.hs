module Variadassist.WebPlayer.Types.OverlayAlerts (
	OverlayAlert(..),
	OverlayAlertRequest(..),
	oarAlert,
	oarRR
) where

import Utils.Messaging hiding (err, message, warn)

import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption(..))

data OverlayAlert = OverlayAlert{
	oaName :: String,
	oaImageURL :: String,
	oaSoundPath :: Maybe FilePath,
	oaVolume :: Int }

data OverlayAlertRequest = MyOverlayAlert OverlayAlert |
	OverlayAlertReward OverlayAlert RewardRedemption

oarAlert :: OverlayAlertRequest -> OverlayAlert
oarAlert (MyOverlayAlert alert) = alert
oarAlert (OverlayAlertReward alert _) = alert

oarRR :: OverlayAlertRequest -> Maybe RewardRedemption
oarRR (MyOverlayAlert _) = Nothing
oarRR (OverlayAlertReward _ rr) = Just rr

instance Messagable OverlayAlert where
	msg = oaName

instance Messagable OverlayAlertRequest where
	msg (MyOverlayAlert alert) = "your " ++ msg alert
	msg (OverlayAlertReward alert rr) = msg alert ++ " via " ++ msg rr
