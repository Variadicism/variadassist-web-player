module Variadassist.WebPlayer.Types.MacroBoard (
	KeyPressAction(..),
	MacroBoardBindings,
	MacroBoardMode(..)
) where

import Utils.Fallible.Trans

import Data.Map (Map)
import Evdev.Codes (Key)

data KeyPressAction =
	SwitchMode {
		kpaName :: String,
		kpaNewMode :: MacroBoardMode } |
	Perform {
		kpaName :: String,
		kpaAction :: VoidFallible }

type MacroBoardBindings = Map Key KeyPressAction
data MacroBoardMode = MacroBoardMode String MacroBoardBindings
