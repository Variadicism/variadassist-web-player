module Variadassist.WebPlayer.Types.Commands (
	Command(..)
) where

import Variadassist.WebPlayer.Types.Music (MusicCategory(..))

data Command =
	MakeReward String Int |
	StopMusic |
	PlayCategory MusicCategory |
	ShowMeme String |
	ShowMyMeme |
	JudgeNextMeme Bool |
	ShowOverlayAlert String |
	OverlayAlertInquire |
	PlayCategoryInquire |
	DisplayRandomizer String |
	SpinRandomizer String deriving (Read, Show)
