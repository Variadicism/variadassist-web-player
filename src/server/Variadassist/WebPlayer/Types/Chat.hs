{-# LANGUAGE RecordWildCards #-}

module Variadassist.WebPlayer.Types.Chat (
	twitchIRCOrigin,
	ircChannel,
	IRCMessageOrigin(..),
	IRCMessage(..),
	simpleIRCMessage,
	parseIRCString,
	toIRCString
) where

import Utils.Fallible
import Utils.Lists (ifNotNull)
import Utils.Maps (intercalateMap, splitToMap)
import Utils.Messaging (msg)
import Variadassist.Twitch.Secrets (streamerUsername)

import Data.Char (isDigit, isLetter, isSpace, isUpper, toLower)
import Data.List (intercalate)
import Data.List.Extra (splitOn, trimEnd, trimStart)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Tuple.Extra (first)

-- constants

twitchIRCOrigin = "tmi.twitch.tv"

ircChannel :: String
ircChannel = "#" ++ map toLower streamerUsername

-- types

data IRCMessageOrigin = Twitch | TwitchUser String | OtherSender String

instance Show IRCMessageOrigin where
	show Twitch = "Twitch"
	show (TwitchUser username) = "@" ++ username
	show (OtherSender sender) = sender

data IRCMessage = IRCMessage{
	ircmCommand :: [String],  -- not empty!
	ircmArgs :: [String],
	ircmTags :: Map String String,
	ircmOrigin :: Maybe IRCMessageOrigin } deriving (Show)

-- utilities

simpleIRCMessage :: [String] -> [String] -> IRCMessage
simpleIRCMessage command args = IRCMessage{
	ircmCommand = command,
	ircmArgs = args,
	ircmTags = Map.empty,
	ircmOrigin = Nothing }

parseIRCString :: String -> Fallible IRCMessage
parseIRCString message = do
	let (tags, afterTags) = case message of
		'@':tagsAndMore -> do
			let (tagsString, afterTagsWithLeadingSpace) = break isSpace tagsAndMore
			let tags = splitToMap "=" ";" tagsString
			(tags, trimStart afterTagsWithLeadingSpace)
		_ -> (Map.empty, message)

	let (origin, afterOrigin) = case afterTags of
		':':originAndMore -> do
			let (originString, afterOriginWithLeadingSpace) = break isSpace originAndMore
			let origin = if "tmi.twitch.tv" == originString
				then Twitch
				else do
					let username = takeWhile (/='!') originString
					if '!' `elem` originString && all (\c -> c == '_' || isLetter c || isDigit c) username
						then TwitchUser username
						else OtherSender originString
			(Just origin, trimStart afterOriginWithLeadingSpace)
		_ -> (Nothing, afterTags)

	let (command, afterCommand) = first (splitOn " " . trimEnd) $
		span (\c -> isSpace c || isUpper c || isDigit c || c == '*') afterOrigin
	assert (not $ null command) $ "No command was found while parsing " ++ msg afterOrigin ++ "!"

	let readArgsFrom argsString = case argsString of
		"" -> []
		':':remainder -> [remainder]
		_ -> do
			let (nextArg, remainder) = break isSpace argsString
			nextArg:(readArgsFrom $ trimStart remainder)
	let args = readArgsFrom afterCommand

	return IRCMessage{
		ircmCommand = command,
		ircmArgs = args,
		ircmTags = tags,
		ircmOrigin = origin }

toIRCString :: IRCMessage -> String
toIRCString IRCMessage{..} = do
	let tagsPrefix = ifNotNull ("@"++) $ intercalateMap "=" ";" ircmTags
	let originPrefix = case ircmOrigin of
		Nothing -> ""
		Just Twitch -> ":" ++ twitchIRCOrigin
		Just (TwitchUser username) -> ":" ++ username ++ "!" ++ username ++ "@" ++ username ++ ".tmi.twitch.tv"
		Just (OtherSender originString) -> ":" ++ originString

	let terms = filter (not . null) [tagsPrefix, originPrefix] ++ (
		if null ircmArgs then ircmCommand
			else ircmCommand ++ init ircmArgs ++ [":" ++ last ircmArgs])

	intercalate " " terms
