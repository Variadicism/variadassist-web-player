module Variadassist.WebPlayer.Types.State (
	ViewerData(..),
	State(..)
) where

import Utils.HATs
import Utils.Processes
import Utils.Queues
import Utils.WebSockets

import Variadassist.WebPlayer.Types.Goals (Goal)
import Variadassist.WebPlayer.Types.MacroBoard (MacroBoardMode)
import Variadassist.WebPlayer.Types.Memes (Meme)
import Variadassist.WebPlayer.Types.OverlayAlerts (OverlayAlertRequest)
import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption)
import Variadassist.WebPlayer.Web.Types.Timers (WebTimer, WebTimerPrototype)

import Data.Map (Map)

data ViewerData = ViewerData {
	intros :: Map String String }
	deriving (Read, Show)

data State = State{
	-- connections
	controlPanelSocket :: WebSocket,
	overlaySocket :: Maybe WebSocket,
	-- external data
	chatAuthToken :: String,
	streamAuthToken :: String,
	streamTitle :: Maybe String,
	headphonesSink :: Maybe Int,
	-- tasks & processes
	commandPipeReadProcess :: Maybe HATProcess,
	controlPanelChromeProcess :: HATProcess,
	maybeMusicTask :: Maybe HAT,
	maybeCalculatorProcess :: Maybe HATProcess,
	-- memes
	memePreviewQueue :: Queue Meme,
	memeDisplayQueue :: TimedQueue Meme,
	-- text-to-speech
	textToSpeechPreviewQueue :: Queue (String, RewardRedemption),
	textToSpeechPlayQueue :: Queue (String, RewardRedemption),
	-- overlay alerts
	overlayAlertsQueue :: Queue OverlayAlertRequest,
	-- display
	goals :: [Goal],
	timerPrototypes :: [WebTimerPrototype],
	timers :: [WebTimer],
	macroBoardMode :: MacroBoardMode,
	-- user data
	viewerData :: ViewerData }
