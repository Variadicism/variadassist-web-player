{-# LANGUAGE RecordWildCards #-}

module Variadassist.WebPlayer.Types.Randomizer (
	Randomizer(..),
	Weight,
	defaultWeightsFor,
	defaultWeightsForCount,
	weightsFilePath
) where

import Utils.Messaging (Messagable, msg)
import Utils.Lists (showWithOr)

import Variadassist.WebPlayer.Instances ()
import Variadassist.WebPlayer.Paths (dataDir)

import Data.IORef (IORef)
import Evdev.Codes (Key)
import Numeric.Extra (intToFloat)
import System.FilePath ((</>))

-- types

type Weight = Float

data Randomizer =
	EvenRandomizer {
		randomizerName :: String,
		randomizerKey :: Key,
		randomizerEntries :: [String],
		randomizerShowTitles :: Bool } |
	WeightedAdjustingRandomizer {
		randomizerName :: String,
		randomizerKey :: Key,
		randomizerEntries :: [String],
		randomizerShowTitles :: Bool,
		randomizerWeightsRef :: IORef [Weight] }

-- instances

instance Show Randomizer where
	show r = msg (randomizerName r) ++ " (" ++ msg (randomizerKey r) ++ ")" ++
		(maybe "" ((" saved in "++) . msg) $ weightsFilePath r) ++
		" choosing " ++ showWithOr (randomizerEntries r)

instance Messagable Randomizer where
	msg = show

-- exported utils

defaultWeightsForCount :: Int -> [Weight]
defaultWeightsForCount entryCount = replicate entryCount $ 100 / intToFloat entryCount

defaultWeightsFor :: Randomizer -> [Weight]
defaultWeightsFor = defaultWeightsForCount . length . randomizerEntries

weightsFilePath :: Randomizer -> Maybe FilePath
weightsFilePath EvenRandomizer{..} = Nothing
weightsFilePath WeightedAdjustingRandomizer{..} = Just $ dataDir </> randomizerName ++ ".hsd"
