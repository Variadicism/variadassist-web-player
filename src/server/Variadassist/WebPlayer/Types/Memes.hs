{-# LANGUAGE RecordWildCards #-}

module Variadassist.WebPlayer.Types.Memes (
	chooseMemeRewardID,
	Meme(..)
) where

import Utils.Messaging

import Variadassist.WebPlayer.Web.Types.PubSub (RewardRedemption(..))

import Data.Maybe (isJust)

-- constants

chooseMemeRewardID = "9c9a6bbe-22de-4a07-846f-6b90d244ee15"

-- types

data Meme = Meme {
	memeUser :: String,
	memeURL :: String,
	memeDisplayURL :: String,
	memeRR :: Maybe RewardRedemption }

-- instances

instance Messagable Meme where
	msg Meme{..} = "@" ++ memeUser ++ "'s " ++ (if isJust memeRR then "reward " else "") ++
		memeURL ++ (if memeURL == memeDisplayURL then "" else " (" ++ memeDisplayURL ++ ")")
