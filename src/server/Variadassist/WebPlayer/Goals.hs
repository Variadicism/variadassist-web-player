module Variadassist.WebPlayer.Goals (
	failGoal,
	meetGoal,
	setGoal
) where

import Utils.Fallible.Trans hiding (elseErr)
import Utils.Messaging (msgShow)

import Variadassist.WebPlayer.State (getState, statefully)
import Variadassist.WebPlayer.Stream (markStream)
import Variadassist.WebPlayer.Types.Goals
import Variadassist.WebPlayer.Types.State (State(..))
import Variadassist.WebPlayer.Utils (playSound)
import Variadassist.WebPlayer.Web.ControlPanel (elseWarn)
import Variadassist.WebPlayer.Web.Types.Overlay (MessageToOverlay(FailGoalWebMessage, MeetGoalWebMessage, SetGoalWebMessage))
import Variadassist.WebPlayer.Web.Utils (sendJSONToPanels)

import Control.Monad (when)

-- private utils

endGoalWith :: String -> (String -> MessageToOverlay) -> FilePath -> VoidFallible -> Goal -> VoidFallible
endGoalWith completionDescription messageMaker soundEffect onComplete goal@Goal{..} = do
	State{..} <- tryIO getState
	if goal `elem` goals then do
		when gMarked $ elseWarn $ markStream $ completionDescription ++ " goal " ++ msgShow goal
		sendJSONToPanels $ messageMaker gName
		playSound soundEffect pencilSoundEffectsVolume
		tryIO $ statefully \state -> state{ goals = filter (/=goal) goals }
		onComplete
	else
		errorT $ "There is no " ++ msgShow goal ++ " to end!"

-- exported utils

setGoal :: Goal -> VoidFallible
setGoal goal@Goal{..} =
	if length gDisplayName > goalMaxDisplayLength then
		errorT $ "Goals can only display up to " ++ show goalMaxDisplayLength ++ " characters!"
	else do
		State{..} <- tryIO getState
		if goal `elem` goals then
			errorT $ "A goal called \"" ++ msgShow goal ++ "\" already exists!"
		else do
			when gMarked $ elseWarn $ markStream $ "set goal " ++ msgShow goal
			sendJSONToPanels $ SetGoalWebMessage goal
			playSound pencilWriteSoundPath pencilSoundEffectsVolume
			tryIO $ statefully $ \state -> state{ goals = goal:goals }

failGoal :: Goal -> VoidFallible
failGoal goal@Goal{..} = endGoalWith "fail" FailGoalWebMessage pencilCrossOutSoundPath gOnFail goal

meetGoal :: Goal -> VoidFallible
meetGoal goal@Goal{..} = endGoalWith "meet" MeetGoalWebMessage pencilCheckBoxSoundPath gOnMeet goal
